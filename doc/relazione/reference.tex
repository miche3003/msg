\documentclass[a4paper,11pt]{article}

\usepackage[utf8x]{inputenc}
\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{fullpage}

\title{\textbf{MSG}\\
Relazione sul progetto del corso di \\
Sistemi Operativi e Laboratorio\\
Docente: Susanna Pelagatti \\
a.a. 2009/10}
\author{Candidato: Michele Carignani}

\begin{document}

\maketitle

\tableofcontents

\section*{Nota}
Questa relazione ha come scopo l' essere d'aiuto alla comprensione 
dello sviluppo del progetto del corso. E' da consultare previa lettura della specifica
del progetto ed è affiancata dalla documentazione in formato Doxygen. 
Poiché disponibili in quest'ultima,
ulteriori informazioni su variabili, strutture dati e funzioni globali 
(file di definizione, campi, funzioni 
chiamanti, etc.) non sono ripetute nella presente relazione. 
Per indicazioni su come generare la documentazione cfr \ref{Doxygen}.

\section{Il server msgserv}

\subsection{Scelte implementative principali}

\paragraph{Tabella utenti} La struttura e la modalità di popolamento
della tabella utenti \verb!UsersTable! sono definite nella specifica. 
Il campo payload di un elemento è una struttura \verb!userinfo!, che tiene traccia dello stato
di connessione dell'utente, del descrittore della socket su cui un utente è connesso, 
del thread di tipo \verb!worker! che ne gestisce le richieste ed infine di una copia 
del suo nickname. 
     
\paragraph{Lista utenti}
\label{lista_utenti}
Parallelamente alla tabella utenti, viene utilizzata la lista
\verb!Broadcast! per implementare il supporto all'invio dei messaggi \verb!MSG_BDCAST! e
\verb!MSG_LIST!. La lista è implementata attraverso il tipo \verb!list_t! fornito dalla 
libreria \verb!libmsg! (cfr \ref{libmsg}). Ogni elemento della lista rappresenta un utente connesso; la chiave 
di un elemento della lista è una copia del nickname dell'utente e il payload è una copia della 
\verb!userinfo! dell'utente presente nella tabella hash, creata al momento delle connessione.

La scelta di utilizzare due strutture parallele per la gestione degli utenti, del loro 
stato e delle loro socket di connessione, è stata dettata da una previsione del comportamento
in generale del server: poiché si presume che le azioni di connessione e
disconnessione di un utente siano relativamente poche rispetto a quelle di invio di
messaggi in broadcast, \textbf{l'accesso con complessità lineare in tempo al descrittore di
ciascun utente connesso è efficace nell'abbattimento dei tempi di risposta del sistema}.

La tabella e la lista sono mantenute aggiornate durante l'esecuzione e vi si accede e si
modificano, tranne in condizioni particolari (es: stato di avvio o arresto del server),
in mutua esclusione, utilizzando il semaforo \textit{TAB} tramite le 
funzioni \verb!LoadUsers(), setOffline(), setOnline()!.

\paragraph{Buffer del writer} 
La struttura per la comunicazione fra i thread \verb!worker!ed il thread \verb!writer!
è stata implementata attraverso un buffer sul modello di cooperazione produttori-consumatore. 
Sono stati definiti il tipo \verb!event_t! che descrive un evento accaduto nel server 
(es. l'invio di un messaggio privato) ed il tipo \verb!eventsBuffer! che descrive una 
sequenza di eventi con riferimenti al primo ed all'ultimo elemento inserito
e ad un contatore degli elementi presenti.

\paragraph{Gestione dei thread} Esistono 3 tipi di thread nel server:
\begin{itemize}
 \item \textbf{main} il thread che attiva e inizializza il server, attiva gli altri thread, accetta nuove connessioni, 
 gestisce l'arrivo di segnali dall'esterno, termina il server;
 \item \textbf{worker} il thread che gestisce le richieste di un particolare utente connesso al server.
  Viene attivato alla connessione dell'utente e termina quando il server viene terminato o l'utente si disconnette;
 \item \textbf{writer} il thread che scrive sul file di log la traccia di tutti i messaggi gestiti durante l'esecuzione del server.
\end{itemize}

Per prevenire attacchi DoS si pone un numero massimo di connessioni accettabili.

\paragraph{Gestione dei segnali}
Segnali gestiti con un handler particolare:
\begin{itemize}
 \item SIGTERM, SIGINT: hanno l'effetto di terminare gentilmente il server. Vengono ricevuti del thread main
 e gestiti attraverso la funzione \verb!CloseUpHandler! che si occupa di innescare la chiusura gentile del server 
 (cfr \ref{terminazione});
 \item SIGSEGV : ha come effetto la stampa di un errore e l'immediata chiusura del server. 
  Può essere catturato da qualsiasi thread (poiché il thread che lo riceve è lo stesso che ha
  generato l'errore) e gestito dalla funzione \verb!GotErrorHandler!;
 \item SIGUSR1 : viene gestito con un handler vuoto definito all'interno del progetto.
\end{itemize}

Segnali ignorati:
\begin{itemize}
 \item SIGPIPE : viene installato l'handler \verb!SIG_IGN!.
\end{itemize}

Tutti gli altri segnali vengono gestiti dagli handler di default.

\paragraph{Terminazione gentile}
\label{terminazione}
Un segnale di terminazione gentile viene catturato dal thread \verb!main! che deve ''propagarlo''
a tutti gli altri thread attivi, prima di terminare. Questo avviene in due modi:
\begin{itemize}
 \item per quanto riguarda i thread worker, lo stato di terminazione è determinato dal controllo
 di una variabile globale chiamata \verb!TimeToClose!, 
  tale che quando \verb!TimeToClose! vale \textit{true} il worker
 termina gentilmente (la variabile è la guardia del ciclo di esecuzione), segnalando la 
 disconnessione e deallocando la memoria. Nel caso in cui il thread sia sospeso su una chiamata
 \verb!read()! viene ''sbloccato'' attraverso l'invio di un segnale \verb!SIGUSR1!;
 \item il thread \verb!writer! termina invece nel momento in cui trova nel buffer degli eventi un evento
 di tipo \verb!MSG_EXIT_EV!.
\end{itemize}

Il meccanismo di terminazione gentile è innescato (come da specifica)
dalla ricezione di un segnale di tipo SIGINT o SIGTERM. Il segnale viene 
ricevuto dal thread \verb!main! e gestito dalla funzione \verb!CloseUpHandler! 
che ha il compito di settare al valore \textit{true} la variabile 
globale \verb!TimeToClose!. In seguito il thread \verb!main! esegue la funzione
ServerOff per terminare i thread bloccati (ciclando sulla lista \verb!Broadcast!),
segnalare la terminazione al thread \verb!writer!, fare pulizia e terminare.

\paragraph{Trattamento degli errori}
\label{tratt_err}
Per riuscire a trattare sistematicamente gli errori delle chiamate di sistema 
sono state definite delle macro
sulla falsa riga di quelle definite in \textit{Unix Advanced Programming} (Rochkind).
Tuttavia il trattamento è molto più semplice perché si limita a deviare il flusso di controllo
ad un blocco di cleanup nella funzione cui viene ritornato errore. E' anche disponibile una
stampa della riga, del file e del chiamante della procedura che ha dato errore, oltre alla
stampa della stringa di errore associata (tramite \verb!perror()! ). Tali informazioni 
sugli errori
non sono attive di default (perché ritenute poco significative per un utente finale)
ma possono essere ''attivate'' attraverso una diversa compilazione del server (cfr \ref{CompServer}).

\subsection{Strutturazione del codice}
Le funzioni che definiscono il comportamento dei thread e le funzioni che accedono a dati globali del server 
sono contenute nei file \verb!worker.c! e \verb!msgsrv.c! (mentre i loro prototipi si trovano nei rispettivi
file di header). Le macro e le variabili globali del server sono definite in \verb!msgserv_defs.h!.

Sono presenti altri tre ''moduli'' del programma, indipendenti fra loro:
\begin{itemize}
 \item \verb!usersTable.o! che fornisce strutture e funzioni per la gestione della tabella utenti (una sorta di 
	interfaccia con la libreria \verb!libmsg!). In particolare incapsula il tipo \verb!userinfo! e rende la struttura
	della tabella opaca agli altri moduli.
 \item \verb!logging.o! che fornisce strutture e funzioni per la funzione di log dei messaggi, in particolare
	incapsula i tipi \verb!event_t! e \verb!eventsBuffer! e implementa il modello \textit{produttori-consumatore} tra i
	thread \verb!worker! ed il thread \verb!writer!.
\item \verb!sigs.o! che fornisce funzioni per la gestione dei segnali, l'installazione
	dei gestori e delle maschere.
\end{itemize}

Sono presenti due file di header, comuni a tutti i file del server:
\begin{itemize}
 \item{\verb!common_hdrs.h!} che include la maggior parte delle librerie esterne utilizzate
  (libreria standard, UNIX standard, etc.);
 \item{\verb!errors.h!} che definisce le macro per il trattamento degli errori in tutte le
  chiamate di sistema presenti nel codice del server (cfr \ref{tratt_err}).
\end{itemize}


Infine vengono utilizzate due librerie esterne sviluppate nell'ambito del progetto MSG:
\begin{itemize}
 \item \verb!libcomsock! per la comunicazione  su socket AF\_UNIX (cfr \ref{comsock});
 \item \verb!libmsg! che implementa tabelle hash e liste generiche (cfr \ref{libmsg}).
\end{itemize}

\subsection{Comportamento del thread main (msgsrv.c)}
\begin{enumerate}
\item Avvio,
\item Controllo degli argomenti riga di comando,
\item Inizializzazione (\verb!ServerInit()!):
	\begin{enumerate}
		\item installazione dei gestori di segnali,
		\item assegnazione \textit{false} a \verb!TimeToClose!,
		\item apertura file log,
		\item apertura file utenti,
		\item creazione e popolamento tabella utenti (\verb!LoadUsersTable()!),
		\item creazione lista utenti attivi,
	\end{enumerate}
\item Creazione della socket del server (\verb!createServerSocket()!),
\item Ciclo finche' non \verb!TimeToClose!:
	\begin{enumerate}
	\item accetta una connessione sulla socket,
	\item se si e' raggiunto il massimo limite di utenti invia un messaggio di errore 
		al client e chiude la connessione, altrimenti attiva nuovo \verb!worker!,
	\end{enumerate}
\item Spegnimento del server (\verb!ServerOff()!):
	\begin{enumerate}
		\item raccoglie gli stati di chiusura dei thread \verb!worker! attivi,
		\item invia messaggio di chiusura al \verb!writer! e raccoglie lo stato di chiusura,
		\item chiude il file di log,
		\item dealloca la tabella utenti (\verb!DestroyUsersTable()!),
		\item dealloca la lista utenti attivi,
		\item chiude la socket del server,
	\end{enumerate}
\item terminazione.
\end{enumerate}

\subsection{Comportamento del thread worker (worker.c)}
    \begin{enumerate}
      \item Mascheramento di tutti i segnali eccetto SIGUSR1,
      \item autenticazione del client connesso, con eventuale aggiornamento
	della tabella utenti e della lista utenti connessi (\verb!authVerify()!),
      \item ciclo finche' non \verb!TimeToClose!:
	\begin{enumerate}
	\item ricezione di un messaggio,
	\item case sul tipo del messaggio e gestione della richiesta (\verb!sendPriv()!, \verb!sendToAll()! 
	ed altre) con logging di eventuali messaggi inviati (\verb!LogEvent()!),
	\end{enumerate}
     \item segnalazione di utente disconnesso,
     \item pulizia,
     \item terminazione.
	
\end{enumerate}

\subsection{Comportamento del thread writer (logging.c)}
\begin{enumerate}
 \item Avvio,
 \item mascheramento di tutti i segnali,
 \item ricezione e copia del puntatore al file di log,
 \item ciclo infinito:
  \begin{enumerate}
   \item lock sul semaforo \verb!log_mtx_!,
   \item ciclo finchè il buffer è vuoto: sospensione sulla condizione \verb!logger_cond_!,
   \item estrazione di un elemento dal buffer,
   \item se il tipo dell'evento è la terminazione, pulizia e terminazione, altrimenti
	  stampa dell'evento sul file, pulizia della memoria, decremento del contatore del buffer,
	  spostamento di testa e coda del buffer,
   \item signal sulla condition dei produttori \verb!log_full_cond_!,
   \item rilascio del semaforo \verb!log_mtx_!.
  \end{enumerate}

\end{enumerate}


\section{Il client  msgcli}

\subsection{Breve descrizione dell'implementazione}
Durante la sua esecuzione il client attiva 2 thread:
 \begin{itemize}
  \item \verb!main! che si occupa di avviare il client, stabilire la connessione con il server,
	attivare il thread sender, ricevere messaggi dal server e stamparli a video;
  \item \verb!sender! che si occupa di leggere da input, verificare e spedire le richieste dell'utente.
 \end{itemize}

\paragraph{Interpretazione dei comandi}
Si è cercato di dare una caratterizzazione generale dei comandi per rendere più facile
l'aggiunta degli stessi. In particolare i comandi definiti con un \verb!%! iniziale
vengono specificati in un unico array, che viene poi scandito per l'interpretazione
del comando inserito. Per estendere le funzionalità del client aggiungendo un nuovo comando,
si dovrebbe quindi inserire il comando nelle strutture condivise nel file msgcli.h
(\verb!Commands!,\verb!CommandSet!, \verb!CommandCodes!, \verb!SetLength!)
ed aggiungere un ramo case allo switch nella procedura principale di invio (\verb!sender!).

\paragraph{Terminazione} 
La terminazione (gentile) del client può avvenire per 3 cause distinte:
\begin{itemize}
 \item lettura di \verb!EOF! dallo standard input,
 \item invio del comando \verb!%EXIT! da parte dell'utente,
 \item chiusura della socket da parte del server,
 \item ricezione di un segnale di tipo SIGINT o SIGTERM.
\end{itemize}

Nei primi due casi, la terminazione è innescata dal thread \verb!sender!, il quale semplicemente
notifica la chiusura al server (inviando un messaggio \verb!MSG_DISCONNECT!), che a sua volta
provvederà a chiudere la socket, determinando la chiusura del thread \verb!main! del client.

La correttezza della procedura è garantita dal corretto comportamento del server,
che si suppone essere affidabile e seguire il protocollo di interazione specificato da
questa implementazione.

Nel terzo caso, il thread \verb!main! riceve un errore dalla funzione \verb!receiveMessage()! e
prima di terminare, assegna 1 alla variabile \verb!Closing!, invia un messaggio SIG\_USR1 al
thread \verb!sender! e ne raccoglie lo stato di terminazione.

Per l'ultimo caso il comportamento è analogo, ma alla variabile \verb!Closing! viene assegnato
il valore 1 dall'handler del segnale ricevuto. I segnali vengono ricevuti dal thread \verb!main!
il quale, in caso di attesa sulla \verb!receiveMessage()! viene sbloccato e può
segnalare la terminazione (come nel caso precedente) e terminare esso stesso.

\subsection{Features aggiunte rispetto alle specifiche}
\label{FeatureClient}

Le funzioni aggiuntive sono disabilitate di default. Per sapere come includerle durante
il processo di compilazione crf \ref{CompServer}

\subsection{Comandi aggiuntivi del client}

\paragraph{@} 
E' un ulteriore comando di invio di un messaggio privato ad un utente con maggiore brevità
e semplicità di utilizzo rispetto a \%ONE. La sintassi del comando è la stessa del comando \%ONE
(uno spazio vuoto è il separatore):
\begin{verbatim}
@ <destinatario> <corpo del messaggio>
\end{verbatim}

\paragraph{\%HELP}
Invoca esplicitamente la stampa del messaggio di aiuto contenente i comandi possibili e il 
loro significato, che viene stampato in caso di comandi malformati o inesistenti. 

\subsection{Pulizia dello schermo}
Il client è in grado di eseguire la pulizia del terminale da cui è invocato. La pulizia
viene eseguita all'avvio ed all'arresto di MSGClient. L'opzione è implementata attraverso una
chiamata \verb!system("clear")!.

\subsection{Notifiche}

Stampa di informazioni aggiuntive sullo stato interno del client 
(come stato di connessione, messaggio di benvenuto etc.).

\subsection{Messaggi colorati}

Sfruttando la possibilità di stampare dei messaggi colorati offerta dalla maggior 
parte delle implementazioni delle shell tramite le sequenze di escape, 
il client è in grado di colorare in modo differente differenti tipi 
di messaggi per l'utente:

\begin{center}
\begin{tabular}{ll}
rosso & messaggi di errore ricevuti dal server\\
bianco & notifiche sullo stato di esecuzione del client \\
verde & messaggi in broadcast \\
blue & messaggi personali
\end{tabular}
\end{center}

L'implementazione della feature è molto semplice: basta includere i messaggi da colorare 
tra due codici particolari che vengono riconosciuti dalla shell e provocano la stampa 
colorata. Nel caso la shell non supporti le stampe colorate è possibile ricompilare 
il client inserendo la macro \verb!NOCOLORS! per impedire l'inserimento dei codici alla 
testa ed alla fine di ogni messaggio.

\section{La libreria comsock}
\label{comsock}
Le funzioni contenute nella libreria sono state implementate senza particolari difficoltà
né soluzioni particolarmente originali.

Da notare è che le funzioni di invio e ricezione di messaggi effettuano in serie
più di una chiamata di scrittura o lettura sulla socket. Pertanto \textbf{queste funzioni 
devono
essere utilizzate in mutua esclusione su una stessa socket}, per evitare che diverse
chiamate possano rendere non significativo il contenuto della socket, ad esempio
alternando una scrittura per ciascuna.

Sono state definite delle ulteriori macro: 
\begin{itemize}
 \item \verb!MSG_TYPES! per definire l'insieme dei tipi di messaggio ammessi ed eseguire un controllo in fase di
  creazione della struttura del messaggio,
 \item due funzioni di controllo degli errori, con un salto ad un blocco di pulizia e ritorno
  di un codice di errore.
\end{itemize}

Vengono fornite due ulteriori funzioni rispetto a quelle della specifica per facilitare 
l'utilizzo della libreria e cercare di aumentarne la
robustezza: 
\begin{itemize}
 \item \verb!createMessage()! che prende come argomenti il tipo, la lunghezza ed il buffer, e restituisce
  un puntatore ad un nuovo messaggio creato;
 \item \verb!destroyMessage()! che prende un puntatore a puntatore di messaggio, elimina
  la memoria allocata e assegna \verb!NULL! al puntatore al messaggio.
  Il fatto che il puntatore valga \verb!NULL! se e solo se è stata deallocata la memoria, 
  permette di eseguire maggiori controlli a tempo di esecuzione e di evitare memory leak.
\end{itemize}


\section{La libreria msg}
\label{libmsg}
Oltre alle funzioni presenti nella specifica viene fornita una funzione per accedere direttamente
al campo payload di un elemento della tabella hash.

La funzione è chiamata \verb!getPayload()!, prende come argomenti la tabella e la chiave e
restituisce il puntatore al campo payload. E' da notare che questa funzione non rompe
l'opacità dell'implementazione della tabella hash, per cui, sebbene dall'esterno sia possibile
modificare un oggetto contenuto in essa, non si hanno informazioni o riferimenti alla struttura
della tabella hash, che può essere gestita solo tramite le funzioni della libreria \verb!MSG!.

\section{Guida per l'utente}

\subsection{Compilazione degli eseguibili}
\label{CompServer}
Gli eseguibili del server e del client sono generabili tramite il comando make
\begin{verbatim}
$ cd /path/to/MSG/src
$ make clean
$ make msgserv
$ make msgcli 
\end{verbatim}

Per rispettare le specifiche ed in particolare per eseguire con buon esito
i test di verifica, le feature aggiuntive dei due programmi sono disabilitate
per default. Possono però essere utilizzate tramite una diversa compilazione dei programmi,
ovvero con i due comandi
\begin{verbatim}
$ cd /path/to/MSG/src
$ make msgserv-dbg
\end{verbatim}
per ricevere stampe contenenti ulteriori informazioni sugli evetuali errori del server e
\begin{verbatim}
$ cd /path/to/MSG/src
$ make msgcli-full
\end{verbatim}
per tutte le feature del client elencate in \ref{FeatureClient}

\subsection{Generazione delle librerie}
Per generare le librerie eseguire da terminale i  comandi
\begin{verbatim}
$ cd /path/to/MSG/src
$ make libcomsock
$ make lib
\end{verbatim}

\subsection{Generazione della documentazione Doxygen}
\label{Doxygen}
Per generare la documentazione, il software Doxygen deve essere installato sulla macchina.
Quindi si utilizzano i comandi
\begin{verbatim}
$ cd /path/to/MSG/src
$ make docu
$ firefox ../doc/html/index.html
\end{verbatim}

\end{document}
