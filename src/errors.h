/** \file errors.h
 * 	\brief Macro per la gestione degli errori
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 */

#ifndef _ERRORS_H_
#define _ERRORS_H_

#include <errno.h>
/** Apertura blocco di codice di pulizia */
#define EC_CLEANUP_BGN \
	ec_cleanup_bgn: \
	{	

/** Chiusura blocco di codice di pulizia*/
#define EC_CLEANUP_END \
	}


#ifdef EC_PRINT_DBG_INFO

#define EC_CMP( var, errrtn ) \
	{ \
		if ( (var) == (errrtn) ) { \
			fprintf(stderr, "%s:%d, in %s : %s\n",__FILE__, __LINE__, __func__, strerror(errno)); \
			goto ec_cleanup_bgn; \
		} \
	}

#define EC_RETVAL( var ) \
	{ \
		int errrtn; \
		if ( (errrtn = (var)) != 0 ) { \
			fprintf(stderr, "%s:%d, in %s : %s\n",__FILE__, __LINE__, __func__, strerror( errrtn )); \
			goto ec_cleanup_bgn; \
		} \
	}
	
#define EC_NONZERO( var ) \
	{ \
		if ( (var) != 0 ) { \
			fprintf(stderr, "%s:%d, in %s : %s\n",__FILE__, __LINE__, __func__, strerror(errno)); \
			goto ec_cleanup_bgn; \
		} \
	}

#else

/** Macro di gestione errore con codice generico */
#define EC_CMP( var, errrtn ) \
	{ \
		if ( (var) == (errrtn) ) { \
			goto ec_cleanup_bgn; \
		} \
	}

/** Macro di gestione errore con codice diverso da 0 */
#define EC_RETVAL( var ) \
	{ \
		int errrtn; \
		if ( (errrtn = (var)) != 0 ) { \
			goto ec_cleanup_bgn; \
		} \
	}
	
/** Macro di gestione errore con codice diverso da 0 */
#define EC_NONZERO( var ) \
	{ \
		if ( (var) != 0 ) { \
			goto ec_cleanup_bgn; \
		} \
	}

#endif

/** Macro di gestione errore con codice -1 */
#define EC_NEG1( s )  EC_CMP( s ,  -1 )

/** Macro di gestione errore con codice 0 */
#define EC_ZERO( s )  EC_CMP( s, 0 )

/** Macro di gestione errore con codice NULL */
#define EC_NULL( s )  EC_CMP( s, NULL )

/*
#define MALLOC(x) printf("[DBG] malloc : %p\n", (x));
 
#define FREE(x) printf("[DBG] free : %p\n", (x)); free(x); ( x ) = NULL;

*/
/** Trattamento migliorato della malloc */
#define MALLOC(x) (x);
 
/** Trattamento migliorato della free */
#define FREE(x) free(x); ( x ) = NULL;

#endif
