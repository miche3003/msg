/** \file sigs.h
 *	\brief File di header per le funzioni di gestione dei segnali
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 */


#ifndef SIGS_H_
#define SIGS_H_

/** \brief Un gestore di segnale che non esegue alcun comando ma ritorna immediatamente
 * 	
 * 	\param signum il segnale da gestire
 */
void VoidHandler (int signum);

/**	\brief Gestore per segnale che provochino immediata chiusura in seguito ad un errore
 * 
 * 	\param signum il segnale da gestire
 */
void GotErrorHandler(int signum);

/** \brief Installa i gestori necessari per il funzionamento del server
 * 	
 * 	In maniera signal-safe (ovvero settando la maschera di protezione) installa un gestore
 * 	per i segnali che inneschino la chiusura gentile del programma. Installa il SIG_IGN
 * 	al segnale SIGPIPE.
 * 
 * 	\param CloseUpHandler handler da installare ai segnali per la chiusura gentile
 * 
 */
int SetSigHandlers(void CloseUpHandler(int signum));

#endif
