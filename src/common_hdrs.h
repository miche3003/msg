/** \file common_hdrs.h
 *	\brief File di header comune, contiene headers di librerie utilizzate da più file
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 */
 
#ifndef _COMM_HDRS_H_
#define _COMM_HDRS_H_

/* *********************************************
 * ******        Librerie esterne          *****
 * ********************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include <sys/stat.h>
#include <bits/signum.h>
#include <signal.h>


/* *********************************************
 * ******    Librerie e funzioni di MSG    *****
 * ********************************************/
/* Macro di gestione degli errori */
#include "errors.h"
/* Libreria di comunicazione su socket AF_UNIX Comsock */
#include "comsock_net.h"
/* Libreria Msg */
#include "genHash.h"

/** Tipo booleano */
typedef int bool;
/** Valore booleano vero*/
#define true 1
/** Valore booleano falso*/
#define false 0

#endif
