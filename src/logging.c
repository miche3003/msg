/**	\file logging.c
 * 	\brief Funzioni per il log degli eventi
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 */


#include "common_hdrs.h"
#include "logging.h"

/** Procedura per l'inserimento di un nuovo evento nel buffer.
  * 	\param type il tipo dell'evento da inserire
  * 	\param mitt puntatore a stringa contenente il nome del mittente
  * 	\param dest puntatore a stringa contenente il nome del destinatario
  * 	\param body puntatore a stringa contenente il corpo del messaggio
  * 
  * 	\retval 0 on success -1 otherwise
  * 
  */
 int LogEvent(char type, char* mitt, char* dest, char* body){
	event_t* nuovo; /* nuovo evento da loggare */
	
	/* Logging */
	pthread_mutex_lock(&log_mtx_);
	while( (log_buffer.count) == LOG_BUFF_SIZE ){
		pthread_cond_wait(&log_full_cond_, &log_mtx_);
	}
	nuovo = log_buffer.Events + log_buffer.tail;
	nuovo->event = type;
	nuovo->sender = strdup(mitt);
	nuovo->receiver = strdup(dest);
	nuovo->mex = strdup(body);
	log_buffer.count = log_buffer.count + 1;
	log_buffer.tail = (log_buffer.tail + 1) % LOG_BUFF_SIZE;
	pthread_cond_signal(&logger_cond_);
	pthread_mutex_unlock(&log_mtx_);
	
	return 0;
}

/** \brief Descrive il comportamento del thread Writer, che scrive i messaggi sul file di log
 * 	
 * 	\param arg il puntatore alla struttura FILE del file di log
 * 
 * 	\return (void*) 0
 */
void* Writer(void * arg){
	FILE* output_file;
	event_t evento;
	sigset_t set;
	
	sigfillset(&set);
	EC_RETVAL( pthread_sigmask( SIG_BLOCK, &set, NULL ) )
	
	pthread_mutex_init(&log_mtx_, NULL);
	pthread_cond_init(&log_full_cond_, NULL);
	pthread_cond_init( &logger_cond_, NULL);
	
	output_file = (FILE*) arg;
	
	while( 1 ){
		pthread_mutex_lock(&log_mtx_);
		while( (log_buffer.count) == 0 ){
			pthread_cond_wait(&logger_cond_, &log_mtx_);
		}
		evento = log_buffer.Events[log_buffer.head];
		
		/* Evento di chiusura del server */
		if (evento.event == MSG_EXIT_EV){
			free(evento.sender);
			free(evento.receiver);
			free(evento.mex);
			fflush(output_file);
			pthread_exit((void*) 0);
		}
		/* Stampa su file */
		fprintf(output_file, "%s:%s:%s\n", evento.sender, evento.receiver, evento.mex);
		free(evento.sender);
		free(evento.receiver);
		free(evento.mex);
		log_buffer.count = log_buffer.count - 1;
		log_buffer.head = (log_buffer.head + 1) % LOG_BUFF_SIZE;
		pthread_cond_signal(&log_full_cond_);
		pthread_mutex_unlock(&log_mtx_);
	}
	
	EC_CLEANUP_BGN
	EC_CLEANUP_END
	return (void*) 0;
}
