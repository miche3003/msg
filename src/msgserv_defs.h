/** \file msgserv_defs.h
 * 	\brief Defines generali del server
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 * 
 */

#include "logging.h"

/* Funzioni per l'intallazione e la gestione dei segnali */
#include "sigs.h"

#ifndef MSGSERV_DEFS_H_
/** */
#define MSGSERV_DEFS_H_

/** Percorso della socket */
#define SOCKET_NAME "./tmp/msgsock"
/** Cartella contenente la socket */
#define SOCKET_FOLDER "./tmp"


/** Il numero massimo di client connessi contemporaneamente */
#define MAXCLIENTS 1000

/** Lunghezza massima per il buffer di un messaggio */
#define MAXMSGLEN 250

/** Dimensione della tabella hash contenente gli utenti abilitati */
#define TABLESIZE 80
/**  */
#define UNIX_PATH_MAX 108

/** File di log */
FILE* LogFile;

/** La struttura che contiene i riferimenti di tutti gli utenti connessi in un determinato momento
 * 
 * La il campo key punta ad una stringa contenente il nickname dell'utente mentre il campo payload contiene il riferimento
 * alla struttura userinfo dell'utente contenuta nella tabella UsersTable.
 * Questa lista viene utilizzata per l'invio dei messaggi di MSG_BRDCAST e dei messaggi MSG_LIST.
 * 
 *  */
list_t* Broadcast;

/** Mutex globale per l'accesso in mutua esclusione alla tabella UsersTable ed alla lista Broadcast */
pthread_mutex_t TAB;

/** ID del thread writer */
pthread_t writer;

/** Variabile per comicare lo stato di chiusura del server */
volatile sig_atomic_t TimeToClose;

/** La tabella hash contenente tutti gli utenti abilitati alla chat e il loro stato di connessione */
hashTable_t* UsersTable;

/** Il numero di utenti connessi al server */
int Connected;


#endif
