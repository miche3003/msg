/** \file worker.c
 *  \brief Implementazione delle funzioni adoperate per i thread Worker del server
 *  \author Michele Carignani Mat. 439004
 * 
 * Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 *  
 */
 
#include "worker.h"
 
int parsePrivMsgBuff(char* src, char** dest, char** content){
	int length; /* Calcola la lunghezza del corpo del messaggio */
	char* tmp; /* Salva il riferimento da assegnare a content */
	
	if (src == NULL ) {errno = EINVAL; return -1;}
	
	/* Controlla il formato di src, se non contiene spazi o 
		il primo carattere è uno spazio ritorna errore */
	tmp = strchr(src, ' ');
	if (tmp == NULL || tmp == src) { errno = EINVAL; return -1; } 
	
	/* tmp punta al primo carattere ' ', perciò separa destintario e messaggio */
	length = ( tmp - src );
	
	/* src + length */
	*content = tmp + 1; 
	
	EC_NULL( *dest = (char*) malloc(sizeof(char)* (length+1)) )
	
	/* Se '\0' non è presente nella stringa sorgente, strncpy non lo inserisce in dest */
	strncpy(*dest, src, length);
	(*dest)[length] = '\0';
	
	return 0;	
	EC_CLEANUP_BGN
		return -1;
	EC_CLEANUP_END
}

char* makeBody(char* src, char* nick){
	char *body; /* Corpo del messaggio da costruire */
	int length, /* Lunghezza della stringa da restituire */
		i, j, /* contatori */ 
		nick_l, /* lunghezza del nickname */
		src_l; /* lunghezza del corpo del messaggio */
	
	/* lunghezza del corpo
	 * 		[ + nickname + ] + spazio + corpo del messaggio + \0
	 * 		1    strlen()  1     1             strlen()        1   = 4 + strlen() + strlen()
	 * */
	 
	if (src == NULL || nick == NULL ){ errno = EINVAL; return NULL; }
	
	src_l =strlen(src) + 1;
	nick_l =strlen(nick) + 1;
	length = 4 +  src_l + nick_l;
	
	MALLOC( body = (char*) malloc(sizeof(char)*length));
	
	/* Insert prefix - "[<mittente>]" */
	body[0] = '[';
	for(i = 1, j = 0; i < (nick_l); i++, j++){
		body[i] = nick[j];
	}
	body[i] = ']'; i++;
	body[i] = ' '; i++;
	
	/* copying message content*/
	for(j = 0; j < (src_l - 1); i++, j++){
		body[i] = src[j];
	}
	body[i] = '\0';
	
	return body;
}

int sendPriv(int Csock, char* nickname, message_t* msg_in){
	message_t* msg_out; /* messaggio da inviare */
	int res, /* risultato della ricerca nella tabella utenti */
		dest_fd = 0; /* fd della socket del destinatario */
	char *content = NULL, /* puntatore al contenuto del messaggio */
		*body = NULL, /* stringa da inviare come buffer del messaggio */
		*dest = NULL; /* stringa contente il nickname del destinatario */
	pthread_mutex_t client_mtx;
	
	/* estraiamo destinatario e contenuti */
	if (( res = parsePrivMsgBuff(msg_in->buffer, &dest, &content) ) == -1 && errno == EINVAL){
		msg_out = createMessage(MSG_ERROR, 26, "[ERROR] Malformed message\n");
		sendMessage(Csock, msg_out);
		destroyMessage(&msg_out);
		return 0;
	}
	
	/* Visita la tabella degli utenti (in mutua esclusione con altri) thread
		e salva il risultato della ricerca in res */
	pthread_mutex_lock(&TAB);
	res = table_query(UsersTable, dest, &dest_fd, &client_mtx);
	pthread_mutex_unlock(&TAB);
	
	switch(res) {
		case NOUSER : 
			/* Da specifica il caso NOUSER provoca lo stesso errore del caso USR_IS_OFF,
			 * ma questa implementazione permette di scegliere (a compilazione) se distinguere i casi
			 */
			#ifdef NOUSER_NOTIFY
			body = (char*) malloc(sizeof(char) * (strlen(dest) + strlen("[ERROR]  : non esiste")));
			sprintf(body,"[ERROR] %s: non esiste", dest); 
			msg_out = createMessage(MSG_ERROR, strlen(body), body);
			sendMessage(Csock, msg_out); 
			break;
			#endif
		case USR_IS_OFF :
			/* Invia un messaggio di errore al mittente */
			body = (char*) malloc(sizeof(char) * (strlen(dest) + strlen("[ERROR]  : utente non connesso")));
			sprintf(body,"[ERROR] %s: utente non connesso", dest); 
			msg_out = createMessage(MSG_ERROR, strlen(body), body);
			sendMessage(Csock, msg_out); 
			break;
		case USR_IS_ON :
			/* costruzione e invio del messaggio privato */
			body = makeBody(content, nickname); 
			msg_out = createMessage(MSG_TO_ONE, strlen(body), body);
			pthread_mutex_lock(&TAB);
			sendMessage(dest_fd, msg_out);
			pthread_mutex_unlock(&TAB);
			LogEvent(MSG_MESSAGE_EV, nickname, dest, content);
			break;
		default : break;
	}
	
	FREE( dest )
	FREE( body )
	destroyMessage(&msg_out);
	
	return 0;
}

char* createUsersList(){
	elem_t* tmp; /* puntatore per l'iterazione sulla lista */
	char *b = NULL, /* la stringa in cui si costruisce la lista utenti */
		*doubled = NULL; /* puntatore temporaneo per la riallocazione della lista */
	int str_list_len; /* lunghezza della stringa contenente la lista */
	
	pthread_mutex_lock(&TAB);
	str_list_len = Connected * 30; /* Si approssimano 30 caratteri per nickname */
	EC_NULL( b = (char*) malloc(sizeof(char)* str_list_len) )
	b[0] = '\0';
	
	tmp = Broadcast->head;
	while(1){
		/* Se necessario si rialloca la stringa raddoppiandone la lunghezza */
		if ( strlen(b) + strlen(tmp->key) >= str_list_len) {
			str_list_len = str_list_len * 2;
			EC_NULL( doubled = (char*) malloc(sizeof(char)* str_list_len) )
			strncpy( doubled, b, str_list_len);
			free(b);
			b = doubled;
		}
		/* concatena il nuovo nickname e se non è l'ultimo aggiunge uno spazio */
		strcat(b, tmp->key);
		if (tmp->next != NULL) {
			tmp = tmp->next;
			strcat(b, " ");
		} else {
			break;
		}
	}
	pthread_mutex_unlock(&TAB);
	
	return b;
	EC_CLEANUP_BGN
		if (b != NULL)
			free(b);
		pthread_mutex_unlock(&TAB);
		return NULL;
	EC_CLEANUP_END
}

int sendToAll(message_t* msg, char* nickname, char* content){
	elem_t* tmp; /* puntatore per scorrere la lista utenti */
	userinfo* us;
	pthread_mutex_lock(&TAB);
	
	/*Scorre la lista utenti ed invia il messaggio ad ognuno */
	tmp = Broadcast->head;
	while(tmp != NULL){
		us = (userinfo*) tmp->payload;
		
		sendMessage( us->sockfd,msg);
		
		/*  Decommentando la riga riga successiva è possibile evitare il la scrittura sul log di messaggi in broadcast
		 * di un utente verso sè stesso 
		 */
		 /* if (strcmp(nickname, tmp->key) != 0) */
		
		LogEvent(MSG_MESSAGE_EV, nickname,tmp->key,content);
		tmp = tmp->next;
	}
	pthread_mutex_unlock(&TAB); /* **************** fine mutua esclusione ********* */
	
	return 0;
}

int authVerify(int s, char** nick, pthread_mutex_t* mtx){
	message_t* msg; /* messaggi di autenticazione */
	int res; /* esito della ricerca nella tabella */
	
	/* Protocollo di connessione */ 
	msg = createMessage(MSG_OK, 0, NULL);
	if (receiveMessage(s,msg) == -1) { return -1; }
	if (msg->type != MSG_CONNECT  || msg->length == 0 || msg->buffer == NULL || msg->length >= MAXNICKLEN ) { 
		errno = EPROTO;  /* oppure ENOMSG */
		destroyMessage(&msg);
		msg = createMessage(MSG_ERROR, 0, NULL);
		sendMessage(s, msg);
		destroyMessage(&msg);
		return -1; 
	} 
	
	/* salva il nickname dell'utente connesso */
	MALLOC ( (*nick = (char*) malloc(sizeof(char)*strlen(msg->buffer))) )
	strncpy( *nick, msg->buffer, strlen(msg->buffer) + 1);
	
	if (strchr(*nick, '\0') == NULL) 
		printf("Non c'è termintatore in nick %s!!\n", *nick);
	
	destroyMessage(&msg);
	
	/* Ricerca dell'utente nella tabella (in mutua esclusione con altri thread) */
	pthread_mutex_lock(&TAB);
	res = table_query(UsersTable, *nick, NULL, mtx);
	pthread_mutex_unlock(&TAB);
		
	switch(res) {
		case NOUSER: 
			msg = createMessage(MSG_ERROR, 17, "User not allowed\n"); 
			sendMessage(s, msg);
			destroyMessage(&msg); 
			return -1; 
		case USR_IS_ON : 
			msg = createMessage(MSG_ERROR, 16, "User already in\n");
			sendMessage(s, msg); destroyMessage(&msg);
			return -1;
		case USR_IS_OFF :
			msg = createMessage(MSG_OK, 0, NULL);
			sendMessage(s, msg);
			destroyMessage(&msg);
			
			/* Segnala lo stato connesso dell'utente */
			pthread_mutex_lock(&TAB);
			if ( setOnline(UsersTable, Broadcast, *nick, s) ==  -1) 
				return -1;
			/*incrementa il numero degli utenti connessi */
			Connected++; 	
			pthread_mutex_unlock(&TAB);
			break;
	}
	return 0;
}

void* Worker(void * arg){
	message_t* msg_in, /* messaggi via via ricevuti */
			*msg_out; /* messaggi via via inviati */
	char* nickname = NULL, /* Una copia locale del nickname (allocata da authVerify) */
		*body; /* */
	int client_sfd, /* fd della socket del client da gestire*/
		k; /* Variabile ausiliaria */
	sigset_t set;
	pthread_mutex_t client_mtx; /* Mutua esclusione sulla socket del client */
	
	/* Mascheramento dei segnali */
	EC_NEG1( sigemptyset(&set) )
	EC_NEG1( sigaddset( &set, SIGINT ) )
	EC_NEG1( sigaddset( &set, SIGTERM ) )
	EC_NEG1( sigaddset( &set, SIGPIPE ) )
	EC_NEG1( pthread_sigmask(SIG_SETMASK, &set, NULL) )
	
	/* Copia locale del file descriptor */
	client_sfd = *((int*) arg);
	
	/* Funzione che implementa il protocollo di autenticazione dell'utente */
	if ((k = authVerify(client_sfd, &nickname, &client_mtx)) == -1) {
		
		/* Se  l'autenticazione fallisce viene chiusa la connessione ed il worker che la gestiva.
			Non ci sono thread che possano raccogliere il valore di ritorno, per cui si esegue la pthread_detach */
		closeSocket(client_sfd);
		pthread_detach(pthread_self());
		pthread_exit((void*) 0);
	}

	
	/* Ciclo di lettura, interpretazione e gestione dei messaggi del client.
		Terminazione :  TimeToClose == 1 ( stato di terminazione )
						errore nella chiamata di receiveMessage()
						ricezione messaggio di tipo MSG_EXIT 					*/
	msg_in = createMessage(MSG_OK, 0, NULL);
	while( ! TimeToClose ) {
		if ((k = receiveMessage(client_sfd, msg_in)) == SEOF || k == -1) break;
		else {
			#ifdef DEBUG
			printf("Ricevuto da %s: %c %s\n", nickname, msg_in->type, msg_in->buffer);
			#endif
			switch (msg_in->type) {
				case MSG_EXIT : 
					msg_out = createMessage(MSG_EXIT, 0, NULL);
					pthread_mutex_lock(&TAB);
		                    	sendMessage(client_sfd, msg_out);
                		    	pthread_mutex_unlock(&TAB);
                    			destroyMessage(&msg_out);
					goto quit;
					break;
				
				case MSG_LIST :  
					body = createUsersList(); 
					msg_out = createMessage(MSG_LIST, strlen(body), body); 
					pthread_mutex_lock(&TAB);
					sendMessage(client_sfd, msg_out); 
					pthread_mutex_unlock(&TAB);
					free(body); break;
					
				case MSG_TO_ONE : 
					sendPriv(client_sfd, nickname, msg_in); break;
					
				case MSG_BCAST:
					body = makeBody(msg_in->buffer, nickname); msg_out = createMessage(MSG_BCAST, strlen(body), body);
					sendToAll(msg_out, nickname, msg_in->buffer);  destroyMessage(&msg_out); free(body); break;
				
				default : break;
			}
			destroyMessage(&msg_in);
			msg_in = createMessage(MSG_OK, 0, NULL);
		}	
	}
	
	/* Cleanup e chiusura*/
	quit: 

	/* Solo nella terminazione del server si esegue la join, perciò se
		il client ha chiuso il thread che lo gestisce deve essere detached */
	if ( k == SEOF ) { pthread_detach(pthread_self()); }
	
	pthread_mutex_lock(&TAB);
	setOffline(UsersTable, Broadcast, nickname);
	Connected--;
	pthread_mutex_unlock(&TAB);
	
	EC_CLEANUP_BGN
	if (msg_in != NULL ) free(msg_in);
	free(nickname);
	closeSocket(client_sfd);
	EC_CLEANUP_END
	
	pthread_exit((void*) 0);
	return (void*) 0;
}
