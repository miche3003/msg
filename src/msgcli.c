/**
   \file msgcli.c
   \author Michele Carignani Mat. 439004
   \brief MSG Client implementation
	
	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
	
	
   Utilizzo
   $ ./msgcli username
   * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sched.h>

#include "comsock_net.h"
#include "msgcli.h"

/** \brief Un gestore di segnale che non esegue alcun comando ma ritorna immediatamente
 * 	
 * 	\param signum il segnale da gestire
 */
static void VoidHandler (int signum) {
	return;
}

/** \brief Un gestore dei segnali che provocano la terminazione gentile del client
 * 	
 * 	\param signum il segnale da gestire
 */
static void CloseHandler (int signum) {
	Closing = 1;
	return;
}

/** This function implements the connection and authentication to the MSG Server listening on the socket SOCKNAME
 * 
 *	\return 0 on succes, -1 on error
 */
static int Connect(char* nick) {
	message_t* msg;
	int trials, ServSock;
	
	#ifdef NOTIFY
	printf("%s",ConnectionMessage);	
	#endif
	
	/* Connessione */
	trials = 5;
	while( (ServSock = openConnection(SOCKNAME)) == -1 && trials != 0 ) { trials--; }
	if (ServSock == -1) { 
		#ifdef NOTIFY
		perror("Error opening connection");
		#endif
		return -1; 
	}

	#ifdef NOTIFY
	printf("-- Connected! --\n");	
	#endif

	/* Protocollo di autenticazione*/
	msg = createMessage(MSG_CONNECT, (unsigned int) strlen(nick), nick);
	EC_NEG1( sendMessage(ServSock, msg) )
	EC_NEG1( destroyMessage(&msg) )
	EC_NULL( msg = createMessage(MSG_OK, 0, NULL) )
	EC_NEG1( receiveMessage(ServSock, msg) )
	
	if (msg->type == MSG_ERROR) { 
		printf("%sAuthentication failed%s",RED,BLANK);
		if (msg->length != 0){ 
			printf(": %s\n",msg->buffer);
			EC_NEG1( destroyMessage(&msg) )
		}  else { 
			printf( "\n"); 
			EC_NEG1( destroyMessage(&msg) )
		}
		return -1;
	}
	
	EC_NEG1( destroyMessage(&msg) )
	
	return ServSock;
	
	EC_CLEANUP_BGN
	return -1;
	EC_CLEANUP_END
}

/** Garantisce la correttezza di una richiesta per invio di Private Message (e.g. comando %ONE)
 * 	\param form puntatore alla stringa contenente il comando da verificare
 * 
 * 	\return 0 se la stringa è corretta, -1 altrimenti
 */
int checkPrivForm(char* form){
	char *blank1 = NULL, *blank2 = NULL, *term = NULL;
	
	blank1 = strchr(form, ' ');
		if (blank1 == NULL ) return -1;
	blank2 = strchr(blank1 + 1, ' ');
		if (blank2 == NULL ) return -1;
	term = strchr(blank2 + 1, '\0');
		if (term == NULL ) return -1;
		
	if ( 
		((blank2 - blank1) <= 1) ||
		((term - blank2) <= 1) 
		)
		return -1;
	
	return 0;
}

/** Input text parser. This function parses the text submitted by the user and selects the command to be executed
 *  by the client.
 * 	
 Verifica la correttezza di un messaggio scritto da tastiera
 %LIST, %EXIT, %ONE sono comandi del client
 *  
 *  \param Input
 *  \param SetLength
 * 
 *  \return the command code as defined in CommandCodes
 */
static int InputCheck( char *Input, int SetLength){
	char *com ;
	int i, j, /* Variabili ausiliarie */
		command; /* Valore di ritorno*/
	
	/* Valore predefinito */
	command = COM_BDCAST;
		
	/* Se il comando è vuoto non succede nulla */
	if ( Input[0] == ' ' || Input[0] == '\n' || Input[0] == '\0') command = COM_VOID;
	
	com = (char*) malloc(sizeof(char)*10);
	
	/* Comandi di tipo %XXX */
	if (Input[0] == '%'){
			(void) sscanf(Input, "%s", com);
			
			for(i = 0; i < SetLength; i++)
				if ( (j=strcmp(com, CommandSet[i])) == 0)
					command =  CommandCodes[i];
		
		/* Se il valore non è stato aggiornato (ed è sempre il valore di default) il messaggio è malformato */
		if ( !(command != COM_BDCAST) ) command = COM_ERROR;
	}
	
	if (Input[0] == '@')
		command = COM_ONE;
	
	if (command == COM_ONE ) {
		if (checkPrivForm(Input) == -1)
			command = COM_ERROR;
	}
	
	free(com);
	return command;
}

static void* sender(void * arg){
	int com, SockFd;
	char input[MAXINPUT], msgbd[MAXINPUT], *p, *s;
	message_t * msg_out;
	sigset_t set;
	int i = 0;
	
	bzero(input, MAXINPUT);
	bzero(msgbd, MAXINPUT);
	
	/* Il thread può ricevere SIGTERM e SIGINT */
	EC_NEG1( sigemptyset(&set) )
	EC_NEG1( sigaddset(&set, SIGINT) )
	EC_NEG1( sigaddset(&set, SIGTERM) )
	EC_NEG1( pthread_sigmask(SIG_UNBLOCK, &set, NULL) )
	
	SockFd = *((int*) arg); /* riceve il descrittore della socket */
	
	while( !Closing ){
		bzero(input, MAXINPUT);
		/* Lettura dell'input dallo stdin con gestione dell'errore ed eventuale chiusura */
		if (fgets(input, MAXINPUT, stdin) == NULL) {
			
			/* Notifica della chiusura all'utente */
			#ifdef NOTIFY
			printf("\n%sBye%s\n",WHITE,BLANK); 
			#endif
			
			/* Notifica della chiusura al server di chat */
			msg_out = createMessage(MSG_EXIT, 0, NULL); 
			EC_NEG1( sendMessage(SockFd, msg_out) )
			EC_NEG1( destroyMessage(&msg_out) )
			pthread_exit(0);
		}
		
		/* fgets() inserisce il carattere newline; se presente, lo togliamo */
		if ( (p = (char*) index(input, '\n')) != NULL) { *(p) = '\0'; }
		
		/* Parsing dell'input */ 
		com = InputCheck(input, SetLength); 
		
		switch (com) {
			case COM_VOID : 
				#ifdef NOTIFY
				fprintf(stderr,"[VOID]\n"); 
				#endif
				break;
			case COM_ERROR : 
				#ifdef NOTIFY
				fprintf(stderr,"%s[ERROR]%s\n",WHITE, BLANK);
				#endif
				printf("%s",HelpString);
				break;
			case COM_BDCAST : 
				msg_out = createMessage(MSG_BCAST, (unsigned int) strlen(input), input); 
				EC_NEG1( sendMessage(SockFd, msg_out) )
				#ifdef DEBUG
				printf("Inviato: %c %s\n", msg_out->type, msg_out->buffer);
				#endif
				EC_NEG1( destroyMessage(&msg_out) )
				break;
			case COM_EXIT : 
				#ifdef NOTIFY
				printf("%sBye%s\n",WHITE,BLANK); 
				#endif
				msg_out = createMessage(MSG_EXIT, 0, NULL); 
				EC_NEG1( sendMessage(SockFd, msg_out) )
				EC_NEG1( destroyMessage(&msg_out) )
				/*
				Closing = 1;
				pthread_kill(S, SIGUSR1);
				*/
				pthread_exit(0); 
				break;
			case COM_LIST : 
				msg_out = createMessage(MSG_LIST, 0, NULL); 
				EC_NEG1( sendMessage(SockFd, msg_out) )
				EC_NEG1( destroyMessage(&msg_out) )
				break;
			case COM_ONE :
				s = strchr(input, ' ') + 1;
				msg_out = createMessage(MSG_TO_ONE, (unsigned int) strlen(s), s);
				EC_NEG1( sendMessage(SockFd, msg_out) )
				#ifdef DEBUG
				printf("Inviato: %c %s\n", msg_out->type, msg_out->buffer);
				#endif
				EC_NEG1( destroyMessage(&msg_out))
				break;
			case COM_HELP : 
				printf("%s",HelpString);
				break;
			 default: break;
		 }
		 sched_yield();
		 i++;
	}
	
	EC_CLEANUP_BGN
	(void) pthread_kill(R, SIGUSR1);
	if (msg_out != NULL) destroyMessage(&msg_out);
	return 0;
	EC_CLEANUP_END
}

/**
 * \brief Main Function
 */
int main(int argc, char * argv[]){
	int k, /* variabile ausiliaria */
		ServSock; /* Descrittore della socket su cui connettersi */
	void *status; /* stato di terminazione dei thread */
	char* Nickname; /* il nickname dell'utente */
	message_t* msg;
	struct sigaction sa; /* Per installare signal handlers */
	sigset_t set; /* Per gestire la signal mask */
	
	/* Check command line arguments*/
	if (argc != 2){
		fprintf(stderr, "Usage: %s nickname\n\n",argv[0]); exit(EXIT_FAILURE);
	}
	
	/* Maschera tutti i segnali*/
	EC_NEG1( sigfillset(&set) )
	EC_NEG1( pthread_sigmask(SIG_BLOCK, &set, NULL) )
	
	/* Installa un Handler vuoto per la gestione di SIGUSR1 */
	bzero(&sa, sizeof(sa));
	sa.sa_handler = VoidHandler;
	sigaction(SIGUSR1, &sa, NULL);
	sa.sa_handler = CloseHandler;
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);
	
	/* Maschera per i segnali SIGTERM e SIGINT */
	EC_NEG1( sigemptyset(&set) )
	EC_NEG1( sigaddset(&set, SIGINT ) )
	EC_NEG1( sigaddset(&set, SIGTERM ) )
	EC_NEG1( pthread_sigmask(SIG_SETMASK, &set, NULL) )
	
	/* Pulizia dello schermo */
	#ifdef CLRSCR
	system("clear");
	#endif
	
	/* Salvataggio del nickname */
	Nickname = strdup(argv[1]);
	
	Closing = 0;
	
	#ifdef NOTIFY
	printf("%s%s%s",WHITE,WelcomeMessage,BLANK);	
	#endif
	
	if ( ( ServSock = Connect(Nickname) ) == -1){
		(void) closeSocket(ServSock);
		(void) free(Nickname);
		exit(EXIT_FAILURE);
	}

	/* Create the sender and receiver threads */
	R = pthread_self();
	EC_RETVAL( pthread_create(&S, NULL, &sender, (void*) &ServSock) )

	msg = createMessage(MSG_OK, 0, NULL); 
	while( !Closing ) {
		if ((k = receiveMessage(ServSock, msg)) == SEOF || k == -1) {
			break;
		}		
		switch (msg->type) {
			case MSG_ERROR :  
				printf("%s%s%s\n",RED,msg->buffer,BLANK); break;
			case MSG_LIST : 
				printf("%s[LIST] %s%s\n",YELLOW,msg->buffer,BLANK); 
				break;
			case MSG_BCAST :
				printf("[BCAST]%s%s%s\n",GREEN,msg->buffer,BLANK); 
				break;
			case MSG_TO_ONE :
				printf("%s%s%s\n",BLUE,msg->buffer,BLANK); break;
			case MSG_EXIT :
				Closing = 1;
				break;
			default :  
				printf("%s%s%s\n",GREEN,msg->buffer,BLANK); break;
		}
		EC_NEG1( destroyMessage(&msg) )
		msg = createMessage(MSG_OK, 0, NULL);
	}
	EC_NEG1( destroyMessage(&msg) )
	
	Closing = 1;
	pthread_kill(S, SIGUSR1);
	EC_RETVAL( pthread_join(S, &status) )
	
	(void) closeSocket(ServSock);
	free(Nickname);
	
	#ifdef CLRSCR
	system("clear");
	#endif
	
	exit(EXIT_SUCCESS);
	
	EC_CLEANUP_BGN
	(void) closeSocket(ServSock);
	free(Nickname);
	if (msg != NULL)
		destroyMessage(&msg);
	exit(EXIT_FAILURE);
	EC_CLEANUP_END
}
