/** \file logging.h
 * 	\brief Prototipi e strutture per le funzioni di logging
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 */

#include <pthread.h>

#ifndef _LOGGING_H
#define _LOGGING_H

/** Lunghezza del buffer in cui vengono inseriti gli eventi da loggare */
#define LOG_BUFF_SIZE 30

/** Possible event types */
/** Client connected and authenticated*/
#define MSG_CONNECT_EV 'C'
/** Client sent a message*/
#define MSG_MESSAGE_EV 'M'
/** Client disconnected */
#define MSG_DISCONNECT_EV 'D'
/** Server is closing */
#define MSG_EXIT_EV 'X'

/** Implementa il tipo evento_t degli eventi da scrivere sul file di log. 
 *  Il campo \c receiver è significativo solo se il tipo è MSG_MESSAGE_EC, altrimenti
 * 	può essere null.
*/
typedef struct ev {
	/** Tipo di evento */
	char event;
	/** Mittente del messaggio in un evento MSG_MESSAGE_EV o soggetto di MSG_CONNECT o MSG_DISCONNECT */
	char* sender;
	/** Destinatario del messaggio in un evento MSG_MESSAGE_EV */
	char* receiver;
	/** Corpo del messaggio in un evento MSG_MESSAGE_EV */
	char* mex;
} event_t;

/** Implementa il buffer degli eventi, secondo il paradigma produttore-consumatore.
 * 
*/
typedef struct {
	/** Struttura del buffer */
	event_t Events[LOG_BUFF_SIZE]; 
	/** Indice dell'elemento da estrarre */
	int head;
	/** Indice dell'elemento da inserire*/ 
	int tail;
	/** Numero di eventi presenti nel buffer e da estrarre*/ 
	int count; 
} eventsBuffer;

/** Semaforo per la mutua esclusione sul buffer di log */
pthread_mutex_t log_mtx_;

/** */  
pthread_cond_t log_full_cond_;

/** */
pthread_cond_t logger_cond_;

/** La struttura condivisa per passare i dati al logger */
eventsBuffer log_buffer;   

/** Procedura per l'inserimento di un nuovo evento nel buffer.
  * 	\param type il tipo dell'evento da inserire
  * 	\param mitt puntatore a stringa contenente il nome del mittente
  * 	\param dest puntatore a stringa contenente il nome del destinatario
  * 
  * 	\retval 0 on success -1 otherwise
  * 
  */
 int LogEvent(char type, char* mitt, char* dest, char* body);

/** \brief Descrive il comportamento del thread Writer, che scrive i messaggi sul file di log
 * 	
 * 	\param arg il puntatore alla struttura FILE del file di log
 * 
 * 	\return (void*) 0
 */
void* Writer(void * arg);

#endif
