/**  \file comsock.c
 *   \author Michele Carignani Mat. 439004
 *   \brief Un'implementazione della libreria di comunicazione socket AF_UNIX "comsock"
 *  
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
*/

#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "comsock_net.h"

/* Stampa informazioni utili al debug */
/* #define DEBUG_CSK_ */

int createServerChannel(char* path){
	int sfd; /* file descriptor della socket */
	struct sockaddr_in sa; /* struttura sockaddr della socket */
	
	/* Controllo degli argomenti */
	if (path ==  NULL) { 
		errno = EINVAL; 
		return -1;
	}
	if (strlen(path) > UNIX_PATH_MAX) { 
		errno = EINVAL; 
		return SNAMETOOLONG; 
	}
	
	/* Creazione sockaddr*/
	sa.sin_family = AF_INET;
	sa.sin_port = htons(3003);
	sa.sin_addr = inet_addr(path)
	
	/* Creazione socket e binding*/
	sfd = socket(AF_INET, SOCK_STREAM, 0);
	EC_CSK_NEG1( bind(sfd, (struct sockaddr *) &sa, (socklen_t) sizeof(sa)) )
	EC_CSK_NEG1( listen(sfd, SOMAXCONN) ) 
	
	
	return sfd;
	
	EC_CLEANUP_LABEL
	(void) close(sfd); 
	return -1; 
}

int closeSocket(int s){
	
	/* Condizioni di errore gestite da accept */
	return close(s); 
	
}

int acceptConnection(int s){
	
	/* Condizioni di errore gestite da accept */
	return accept(s, NULL, 0);

}

int receiveMessage(int sc, message_t* msg){
	char t, /* tipo del messaggio ricevuto */
		 *p ; /* */
	unsigned int l = 0; /* */
	int k; /* */
	
	/* Controllo degli argomenti */
	if (msg == NULL) {  
		errno = EINVAL; 
		return -1;  
	}
		/* Se msg->buffer non è NULL, è possibile che riscrivendolo si crei
		  un memory leak, perciò la procedura deve fallire */
	if (msg->buffer != NULL) {  
		errno = EINVAL; 
		return -1;  
	}
	
	/* lettura del tipo  dalla socket*/
	EC_CSK_NEG1( k = read(sc, &(t), sizeof(char)) )
	if (k == 0)
		return SEOF;
	msg->type = t;
	
	/* lettura della lunghezza del buffer */
	EC_CSK_NEG1( k = read(sc, &(l), sizeof(unsigned int)))
	if (k == 0)
		return SEOF;
 	msg->length = l;
	
	if ( l != 0 ) {
		msg->buffer = NULL;
		EC_CSK_NULL( msg->buffer = (char*) malloc(sizeof(char)*(l+1)) )
		/* strlen() +1 per includere il carattere '\0' */
		EC_CSK_NEG1( k = read(sc, (msg->buffer) , l))
		if ( (p = strchr(msg->buffer, '\n')) != NULL) { *(p) = ' '; }
		msg->buffer[l] = '\0';
	}
		
	return l;
	
	EC_CLEANUP_LABEL
	if (msg->buffer != NULL )
		free(msg->buffer);
	return -1; 
}

/** scrive un messaggio sulla socket
 *   \param  sc file descriptor della socket
 *   \param msg struttura che contiene il messaggio da scrivere 
 *   
 *   \retval  n    il numero di caratteri inviati (se scrittura OK)
 *   \retval  SEOF se il peer ha chiuso la connessione 
 *                   (non ci sono piu' lettori sulla socket) 
 *   \retval -1   in tutti gl ialtri casi di errore (sets errno)
 
 */
int sendMessage(int sc, message_t *msg){
	
	/* Controllo degli argomenti */
	if (msg == NULL) { 
		errno = EINVAL; 
		return -1; 
	}
	
	EC_CSK_NEG1( write(sc, &(msg->type), sizeof(char)) )
	EC_CSK_NEG1( write(sc, &(msg->length), sizeof(unsigned int)) )
	if ( msg->length != 0) { 
		EC_CSK_NEG1( write(sc, msg->buffer, (size_t) msg->length) )
	}
	
    return (int) (msg->length);
    
    EC_CLEANUP_LABEL
    if (errno == EPIPE) 
		return SEOF; 
	else 
		return -1;
}

/** crea una connessione all socket del server. In caso di errore funzione tenta NTRIALCONN volte la connessione (a distanza di 1 secondo l'una dall'altra) prima di ritornare errore.
 *   \param  path  nome del socket su cui il server accetta le connessioni
 *   
 *   \return fd descrittore della socket se la connessione ha successo
 *   \return SNAMETOOLONG se il nome del socket eccede UNIX_PATH_MAX
 *   \return -1 negli altri casi di errore (sets errno)
 *
 *  in caso di errore ripristina la situazione inziale: rimuove eventuali socket create e chiude eventuali file descriptor rimasti aperti
 */
int openConnection(char* path){
	int socket_fd, i, e;
	struct sockaddr_in sa;
	
	if (strlen(path) >= UNIX_PATH_MAX) { return SNAMETOOLONG; }
	
	strncpy(sa.sun_path, path,UNIX_PATH_MAX);
	sa.sun_family=AF_INET;
	
	/* creazione socket */
	EC_CSK_NEG1( socket_fd = socket(AF_UNIX, SOCK_STREAM, 0))
	
	e = 0;
	for(i = 0; i < NTRIALCONN; i++){
		if ( (connect(socket_fd, (struct sockaddr *) &sa,  (socklen_t) sizeof(sa))) == 0) { break; }
		e = errno;
		(void) sleep(1);
	}
	
	/* Connection ha fallito NTRIALCONN volte */
	if (e != 0) { (void) close(socket_fd); errno = e; return -1; }
	
	return socket_fd;
	EC_CLEANUP_LABEL
	return -1;
}

message_t * createMessage(char t, unsigned int l, const char* b){
	message_t* m; /* puntatore al messaggio da creare */
	
	/* Controllo della ammissibilità del tipo del messaggio */
	if ( strchr(MSG_TYPES, t) == NULL) {errno = EINVAL; return NULL; }
	if ( l != 0 && b == NULL ) { errno = EINVAL; return NULL; }
	
	EC_CSK_NULL( m = (message_t*) malloc(sizeof(message_t)) )
	
	m->type = t;
	m->length = l;
	
	if (l != 0 ) {
		m->buffer = (char*) malloc(sizeof(char)*(l+1));
		strncpy(m->buffer, b, l);
		m->buffer[l] = '\0';
	} else {
		m->buffer = NULL;	
	}
	
	return m;
	
	EC_CLEANUP_LABEL
	return NULL;
}

int destroyMessage(message_t** msg){
	
	if (msg == NULL || *msg == NULL) { errno = EINVAL; return -1; }
	
	if ( (*msg)->buffer != NULL) { 
		#ifdef DEBUG_CSK_
		printf("[DBG] free(msg->buffer) [destroy]\n");
		#endif
		free((*msg)->buffer); 
	}
	
	#ifdef DEBUG_CSK_
		printf("[DBG] free(msg) [destroy]\n");
	#endif
	free(*msg);
	
	(*msg) = NULL;
	
	return 0;
}
