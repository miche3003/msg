/**
   \file genList.c
   \author Michele Carignani Mat. 439004
   \brief  Implementazione di liste con contenuto generico
	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "genList.h"

/** crea una lista generica
    \param compare funzione usata per confrontare due chiavi 
    \param copyk funzione usata per copiare una chiave
    \param copyp funzione usata per copiare un payload

    \retval NULL in caso di errori con \c errno impostata opportunamente
    \retval p (p!=NULL) puntatore alla nuova lista
 */
list_t *new_List(int (*compare) (void *, void *),void* (* copyk) (void *),void* (*copyp) (void*)){
	
	list_t* list;
	
	/* Controllo della validità degli argomenti */
	if (compare == NULL || copyk == NULL || copyp == NULL){
		errno = EINVAL;
		return NULL; 
	}
	
	/* Allocazione dell'intestazione della lista. Errno eventualmente settato da malloc() */
	list = (list_t *) malloc(sizeof(list_t));
	if ( list == NULL){
		return NULL; 
	}
	
	/* Passaggio delle impostazioni */
	list -> head = NULL;
	list -> compare = compare;
	list -> copyk = copyk;
	list -> copyp = copyp;
	
	return list;
}

/**Distrugge una lista deallocando tutta la memoria occupata
    \param pt puntatore al puntatore della lista da distruggere

    nota: mette a NULL il puntatore \c *t
 
	Non si eseguono controlli sulla corretta esecuzione di free(). Ad ogni modo, l'utente della libreria può controllare se errno != 0 
	dopo l'esecuzione di free_List() per localizzare memory leaks.
 */
void free_List (list_t ** pt){							
	elem_t *victim, *tmp;
	errno = 0;
	
	/* Controllo della validità degli argomenti */
	if (pt ==  NULL) {errno = EINVAL; return;}
	if ( (*pt) == NULL) {return;}
	
	/* dealloca tutti gli elementi */
	for(tmp = victim = (*pt)->head; victim != NULL; victim = tmp){ 					
		tmp = victim->next;
		free(victim->key);
		free(victim->payload);
		free(victim);
	}
	
	/* controllo la corretta escuzione di errno */
	if (errno != 0) {return;}
	
	/* dealloca l'intestazione della lista e ritorna */
	free(*pt);														
	*pt = NULL;
}

/** inserisce una nuova coppia (key, payload) in testa alla lista, 
    sia key che payload devono essere copiate nel nuovo elemento della lista.
    Nella lista \b non sono permesse chiavi replicate

    \param t puntatore alla lista
    \param key la chiave
    \param payload l'informazione

    \retval -1 se si sono verificati errori (errno settato opportunamente)
    \retval 0 se l'inserimento \`e andato a buon fine

	L'inserimento di un elemento già presente è un errore EKEYREJECTED.
 */
int add_ListElement(list_t * t,void * key, void* payload){
	elem_t* tmp;
	
	/* Controllo l'esistenza degli argomenti */
	if (t == NULL || key == NULL || payload == NULL) {
		errno = EINVAL;
		return ERROR;
	}
	
	if (find_ListElement(t,key) != NULL){ errno = EKEYREJECTED; return ERROR;} 
	
	if ((tmp = (elem_t*) malloc(sizeof(elem_t))) == NULL){return ERROR;}
	
	/* Alloco ed inserisco payload e key, con gestione degli errori */
	if ((tmp -> payload = t->copyp(payload)) == NULL){
		free(tmp);
		return ERROR;
	}
	if ((tmp -> key = t->copyk(key)) == NULL){
		free(tmp->payload);
		free(tmp);
		return ERROR;
	}
	
	/* Set dei collegamenti */
	tmp -> next = t -> head;
	t -> head = tmp;
	
	return CORRECT;
}	
	
/** elimina l'elemento di chiave (key) deallocando la memoria

    \param t puntatore alla lista
    \param key la chiave


    \retval -1 se si sono verificati errori (errno settato opportunamente)
    \retval 0 se l'esecuzione e' stata corretta

	La funzione ha successo se al termine dell'esecuzione nella lista puntata da t non esiste un'elemento con chiave *key.
	Non cerca l'elemento da cancellare con find_ListElement() perchè inutile scandire due volte la lista.
*/
int remove_ListElement(list_t * t,void * key){
	
	elem_t *tmp, *prev;
	
	/* Controllo della validità degli argomenti */
	if (key == NULL){
	 	errno = EINVAL;
		return ERROR; 
	}
	
	/* Controllo lista non vuota -  non è un errore*/
	if (t -> head == NULL){ 
		return CORRECT;
	}
	
	/* Ricerca ed eliminazione in testa */
	prev = tmp = t -> head;
	if ((t -> compare)(tmp -> key,key) == 0){
		t->head = tmp->next;
		
		/* Deallocazione */
		free(tmp->key);
		free(tmp->payload);
		free(tmp);
		/* if (errno != 0){return ERROR;} */
		
		return CORRECT;
	}
	/* nella coda */
	tmp = tmp->next;
	while(tmp != NULL){
		if ( (t -> compare) (tmp -> key, key) == 0){
			
			/* Corretto collegamento */
			prev->next = tmp->next;
			
			/* Deallocazione */
			free(tmp->key);
			free(tmp->payload);
			free(tmp);
			/*if (errno != 0){return ERROR;}*/
			
			return CORRECT;
		}
		prev = tmp; tmp = tmp-> next;
	}
	return CORRECT;
}

/** cerca l'elemento di chiave (key)

    \param t puntatore alla lista
    \param key la chiave


    \retval NULL se l'elemento non c'e'
    \retval p puntatore all'elemento trovato (puntatore interno alla lista non alloca memoria)

	Non è errore passare un puntatore a NULL al posto della lista.
*/
elem_t * find_ListElement(list_t * t,void * key){
	elem_t *tmp;
	
	if (t == NULL) { return NULL;}
	
	for(tmp = t -> head; tmp != NULL; tmp = tmp -> next) {
		if ( (t -> compare)(tmp -> key, key) == 0){ return tmp; }
	}
	
	return NULL;
}
