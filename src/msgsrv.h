/**	\file msgsrv.h
 * 		\brief Headers per il server Msg
 * 		\author Michele Carignani Mat. 439004
 *	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 * \mainpage MSG - Un'implementazione di server e client di chat su socket AF_UNIX
 *
 * \section intro_sec Introduzione
 *
 * Il software MSG è stato sviluppato come progetto per il modulo di Laboratorio del 
 * corso di Sistemi Operativi e Laboratorio 20 - Univeristà di Pisa, Corso di Laurea in Informatica
 *
 * Docente: Susanna Pelagatti
 * 
 * Sviluppatore e responsabile di progetto: Michele Carignani
 * 
 * \section spec Specifiche
 * 	
 * 	Le specifiche del progetto sono reperibili qui.
 * 
 * \section Documentazione
 * 
 * 	Oltre alla documentazione doxygen è disponibile avere copia della relazione sull'implementazione
 * 	presso lo sviluppatore
 * 
 * 	\section feat Caratteristiche addizionali
 * 	\subsection Client
 * 	- Notifica agli altri utenti la connessione/disconnessione di un utente
 * 	- Loggin di informazioni aggiuntive  ie. connessione/disconnessione
 *   
 * \section cont Contatti
 * 	Michele Carignani - email: carignani@cli.di.unipi.it web: http://www.cli.di.unipi.it/~carignani/
 * 
 */

#ifndef _MSGSRV_MC_H
#define _MSGSRV_MC_H

#include "common_hdrs.h"
#include "msgserv_defs.h"
#include "usersTable.h"
#include "worker.h"

/** Messaggio di errore se sono connessi troppi utenti */
char TooMany[] = "Too many connections\n";

/** Percorso per il file di log*/
char* LogFilePath;

/** Percorso per il file degli utenti */
char* UserFilePath;

/** \brief Inizializzazione delle strutture dati del server
 * 	Viene creata e popolata la tabella hash UsersTable, viene aperto il file di log
 * 		
 * 	\return 0 on success
 */
int ServerInit();
	
/** Descrive il comportamento del thread Worker, che gestisce una connessione con un
 * 	client MSG.
 * \param arg deve passare la socket su cui è connesso il nuovo client
 * 
 */
void* Worker(void * arg);

/** Operazioni per la corretta terminazione del server MSG.
 *  
 */
int ServerOff(int ServerSocket);


/** \brief Gestore dei messaggi per la terminazione gentile del server
 * 	
 * 	L'unico effetto è settare a true il valore di una variabile globale che determina lo stato
 * 	di chiusura del server
 * 
 *  \param signum il segnale da gestire
 * 
 */
void CloseUpHandler(int signum);
#endif

