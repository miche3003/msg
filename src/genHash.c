/**
   \file genHash.c
   \author Michele Carignani Mat. 439004
   \brief implementazione per tabelle hash con tipi generici, con collisioni gestite con
		liste di trabocco.

   Il contenuto di questo file e' in ogni sua parte opera originale dell' autore

 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <math.h>

#include "genList.h"
#include "genHash.h"

#ifndef ERROR
#define ERROR -1
#endif

#ifndef CORRECT
#define CORRECT 0
#endif

#define HASH_MAGIC 569
#define HASH_MAGIC2 43

/** 
	I 'buckets' sono inizializzati a NULL e la lista di trabocco è creata solo quando si
	un elemento che cade in quel 'bucket'.
	Una lista di trabocco non viene più deallocata fino alla distruzione della tabella.
	La funzione hash deve ritornare un valore fra 0 e \c size
	
 */
hashTable_t * new_hashTable (unsigned int size,  int (* compare) (void *, void *), void* (* copyk) (void *),void* (*copyp) (void*),unsigned int (*hashfunction)(void*,unsigned int)){
	
	hashTable_t * t;		
	int i;
	
	/* Controllo della validità degli argomenti */
	if (compare == NULL || copyk == NULL || copyp == NULL || size <= 0){
		errno = EINVAL;
		return NULL;
	}
	
	/* Struct creation*/
	t = (hashTable_t *) malloc(sizeof(hashTable_t));
	if(t == NULL){return NULL; } 										/* errno settato correttamente da malloc*/
	
	/* Impostazioni della tabella */
	t -> size = size;
	t -> compare = compare;
	t -> copyk = copyk;
	t -> copyp = copyp;
	t -> hash = hashfunction;
	t->count = 0;
	
	/* Creazione del vettore di liste */
	t ->table =  (list_t **) malloc(sizeof(list_t *) * (size));
	if( t->table == NULL){return NULL;}								 /* errno settato correttamente da malloc*/
	
	/* inizializzazione del vettore di liste */
	for(i = 0; i < size; i++){
		(t->table)[i] = NULL; 
	}
	
	return t;
}

/** funzione hash per key di tipo int
   \param key valore chiave
   \param size ampiezza della hash table

   \retval index posizione nella tabella
	
	La funzione è una tipica funzione di hash statico h(k) = (a * k) + b (con a e b costanti prime). Il valore k è costituito dal
	valore intero della chiave. E' distinto il caso in cui a = size: in questo caso la funzione è costante, per cui
	viene restituito semplicemente il resto di k modulo size.

 */
unsigned int hash_int (void * key, unsigned int size){
	unsigned int *h;
	unsigned int hash;
	
	h = ( unsigned int*) key;
	
	if (size != HASH_MAGIC)
		hash = ( (HASH_MAGIC * (*h)) + HASH_MAGIC2) % size;
	else
		hash =(*h) % size;
	
	return hash;
}

/** funzione hash per key di tipo string
   \param key valore chiave
   \param size ampiezza della hash table

   \retval index posizione nella tabella
   
	La funzione è una tipica funzione di hash statico h(k) = (a * k) + b (con a e b costanti prime). Il valore k è costituito dalla
	sommatoria delle lettere della chiave. E' distinto il caso in cui a = size: in quel caso la funzione è costante, per cui
	viene restituito semplicemente il resto di k modulo size.

 */
unsigned int hash_string (void * key, unsigned int size){
	int i, cont, j;
	char* s = (char*) key;
	
	for(cont = i=0; i < strlen(s); i++){
		j = (int) s[i];
		cont += abs(j);
	}
	if (size != HASH_MAGIC)
		cont = ( HASH_MAGIC * cont ) + HASH_MAGIC2;

	return cont % size;
}


/**  distrugge una tabella hash deallocando tutta la memoria occupata
    \param pt puntatore al puntatore della tabella da distruggere

    nota: mette a NULL il puntatore \c *pt
    
	Non è un errore liberare una tabella vuota (ovvero pt == NULL). Non si eseguono controlli sulla corretta esecuzione di free().
	Ad ogni modo, l'utente della libreria può controllare se errno != 0 dopo l'esecuzione di free_hashTable() per localizzare
	memory leaks.

 */
void free_hashTable (hashTable_t ** pt){
	int i, length;
	
	if( pt == NULL) {errno = EINVAL; return;}
	if ( (*pt) == NULL) {return;}
	length = (*pt) -> size;
	
	/* Deallocazione delle liste di trabocco*/
	for(i = 0; i < length; i++){
		if(( (*pt) -> table)[i] != NULL){
			free_List( &(( (*pt) -> table)[i]) );
			if (  ( ( (*pt) -> table)[i]) != NULL) {return;} 
		}
	}
	
	/* Deallocazione struttura lista */				
	free((*pt)->table);
	free(*pt);
	
	/* AnNULLamento del puntatore */
	(*pt) = NULL;
}

/** inserisce una nuova coppia (key, payload) nella hash table (se non c'e' gia')

    \param t la tabella cui aggiungere
    \param key la chiave
    \param payload l'informazione

    \retval -1 se si sono verificati errori (errno settato opportunamente)
    \retval 0 se l'inserimento \`e andato a buon fine
    
	Condizioni di errore: errore in add_ListElement, tentativo di inserimento di un elemento già presente o calcolo errato dell'hash. 
	In caso di errore nell'aggiungere alla lista di trabocco add_ListElement setta il giusto errno. 

 */
int add_hashElement(hashTable_t * t,void * key, void* payload ){

	int pos;	
	void * p;
	
	/* Controlla se elemento è già presente */
	if ((p = find_hashElement( t, key)) != NULL){ free(p); errno = EINVAL; return ERROR;}
	
	/* Calcolo dell'hash e controllo della correttezza del valore ritornato dalla funzione*/
	pos = (t->hash)(key, t->size);
	if(pos < 0 || pos >= (t->size)){
		errno = ERANGE;
		return ERROR;
	}
	
	/* esiste la lista di trabocco? */
	if ( (t->table)[pos] == NULL ) { 
		(t->table)[pos] = new_List(t -> compare, t -> copyk, t->copyp);
	}
	
	/* Inserimento nella lista di trabocco, nota errno è settato da add_ListElement */
	if (( add_ListElement( (t->table)[pos], key, payload) ) == -1) {	
		return ERROR; 
	}
	t->count ++;
	return CORRECT;
}

void * find_hashElement( hashTable_t * t,void * key ){
	int pos;
	elem_t *el;
	void *p;
	
	/* Calcolo dell'hash e controllo della correttezza del valore ritornato dalla funzione */
	pos = (t->hash)(key, t->size);
	if(pos < 0 || pos >= (t->size)){
		errno = ERANGE;
		return NULL;
	}
	
	/* Ricerca nella lista */
	el = find_ListElement((t->table)[pos], key);
	if (el ==NULL) {return NULL;}
	p = (t -> copyp)(el -> payload);
	return p;
}


int remove_hashElement( hashTable_t * t,void * key ){
	int pos;
	void * p;
	
	/* Controllo validità degli argomenti */
	if(key == NULL || t == NULL){errno = EINVAL; return ERROR;}
	
	if(( p = find_hashElement( t, key)) ==  NULL) {return CORRECT;}
	else {free(p);}
	
	/* Calcolo dell'hash e controllo della correttezza del valore ritornato dalla funzione */
	pos = (t->hash)(key, t->size);
	if(pos < 0 || pos >= (t->size)){
		errno = ERANGE;
		return ERROR;
	}
	
	if (remove_ListElement((t->table)[pos],key) == -1){
		return ERROR;
	}
	t->count--;
	return CORRECT;
}

void * getPayload(hashTable_t* t, void * key){
	int pos;
	elem_t *el;
	
	/* Calcolo dell'hash e controllo della correttezza del valore ritornato dalla funzione */
	pos = (t->hash)(key, t->size);
	if(pos < 0 || pos >= (t->size)){
		errno = ERANGE;
		return NULL;
	}
	
	/* Ricerca nella lista */
	el = find_ListElement((t->table)[pos], key);
	if (el ==NULL) {return NULL;}
	return el->payload;
}
