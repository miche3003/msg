/** \file sigs.c
	\author Michele Carignani Mat. 439004
	\brief Gestione dei segnali
	
	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
*/

#include "common_hdrs.h"
#include "sigs.h"

/** Comunicazione di errore grave all'utente
 * 
 */
static char BadErrorMsg[] = "E' stato catturato un errore grave del sistema. \
Maggiori informazioni sono disponibili nel file di log.\n \
Vi prechiamo di contattare il servizio clienti del produttore.\n\n";

void VoidHandler (int signum) {
	return;
}

/**
 * 
 */
void GotErrorHandler(int signum){
	write(2,BadErrorMsg, strlen(BadErrorMsg));
	
	return;
}

int SetSigHandlers(void Handler(int signum)){
	struct sigaction act;
	sigset_t set;
	
	/* Mascheramento dei segnali */
	EC_NEG1( sigfillset(&set) )
	EC_NEG1( sigprocmask(SIG_SETMASK, &set, NULL) )
	
	bzero(&act, sizeof(act));
	
	/* Installazione gestore per segnali di chiusura gentile */
	act.sa_handler = Handler;
	EC_NEG1( sigaction(SIGINT, &act, NULL) );
	EC_NEG1( sigaction(SIGTERM, &act, NULL) );
	
	act.sa_handler = VoidHandler;
	EC_NEG1( sigaction(SIGUSR1, &act, NULL) );
	
	/* Installazione gestore per segnali di errore da trattare */
	act.sa_handler = SIG_IGN;
	EC_NEG1( sigaction(SIGPIPE, &act, NULL) );
	
	EC_NEG1( sigemptyset(&set) )
	EC_NEG1( sigprocmask(SIG_SETMASK, &set, NULL) )
	
	return 0;
	
	EC_CLEANUP_BGN
	return -1;
	EC_CLEANUP_END
}

