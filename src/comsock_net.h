/** \file comsock.h
 *  \author Michele Carignani Mat. 439004
 *  \brief Libreria di communicazione via socket AF_UNIX
 *  
 *  Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
*/

#ifndef _COMSOCK_H
 /** Inclusione di comsock */
 #define _COMSOCK_H

/* -= TYPES =- */

/** <H3>Message</H3>
 * The \c message_t structure represents a message
 *  - \c type message type
 *  - \c length message buffer lenght in bytes 
 *  - \c buffer pointer to the message body (could be NULL if length == 0 )
 *
 * <HR>
 */
typedef struct {
	/** tipo del messaggio */
    char type;
    /** lunghezza in byte */           
    unsigned int length;
    /** buffer messaggio */ 
    char* buffer;        
} message_t; 

/** lunghezza buffer indirizzo AF_UNIX */
#define UNIX_PATH_MAX    108

/** fine dello stream su socket, connessione chiusa dal peer */
#define SEOF -2
/** Error Socket Path Too Long (exceeding UNIX_PATH_MAX) */
#define SNAMETOOLONG -11 
/** numero di tentativi di connessione da parte del client */
#define  NTRIALCONN 5

/** tipi dei messaggi scambiati fra server e client */
/** richiesta di connessione utente */
#define MSG_CONNECT        'C'
/** errore */
#define MSG_ERROR          'E' 
/** OK */
#define MSG_OK             'O'
/** NO */
#define MSG_NO             'N' 
/** messaggio a un singolo utente */
#define MSG_TO_ONE         'T' 
/** messaggio in broadcast */
#define MSG_BCAST          'B' 
/** lista utenti connessi */
#define MSG_LIST           'L' 
/** uscita */
#define MSG_EXIT           'X' 

/** Tipi disponibili per un messaggio*/
#define MSG_TYPES "CEONTBLX"


/* -= TRATTAMENTO ERRORI =- */
/** Macro per la gestione di errori con esito -1
 */
#define EC_CSK_NEG1( x ) \
if ( (x) == -1 ) \
	goto cleanup;

/** Macro per la gestione di errori con esito NULL
 */	
#define EC_CSK_NULL( x ) \
if ( (x) == NULL ) \
	goto cleanup;
	
/** Macro per il posizionamento dell'etichetta al codice di clenaup
 */
#define EC_CLEANUP_LABEL cleanup:

/* -= FUNZIONI =- */
/** Crea una socket AF_UNIX
 *  \param  path pathname della socket
 *
 *  \retval s    il file descriptor della socket  (s>0)
 *  \retval SNAMETOOLONG se il nome del socket eccede UNIX_PATH_MAX
 *  \retval -1   in altri casi di errore (sets errno)
 *
 *  in caso di errore ripristina la situazione inziale: rimuove eventuali socket create e chiude eventuali file descriptor rimasti aperti
 */
int createServerChannel(char* path);

/** Chiude una socket
 *   \param s file descriptor della socket
 *
 *   \retval 0  se tutto ok, 
 *   \retval -1  se errore (sets errno)
 */
int closeSocket(int s);

/** accetta una connessione da parte di un client
 *  \param  s socket su cui ci mettiamo in attesa di accettare la connessione
 *
 *  \retval  c il descrittore della socket su cui siamo connessi
 *  \retval  -1 in casi di errore (sets errno)
 */
int acceptConnection(int s);

/** legge un messaggio dalla socket
 *  \param  sc  file descriptor della socket
 *  \param msg  struttura che conterra' il messagio letto 
 *		(deve essere allocata all'esterno della funzione,
 *		tranne il campo buffer)
 *
 *  \retval lung  lunghezza del buffer letto, se OK 
 *  \retval SEOF  se il peer ha chiuso la connessione 
 *                   (non ci sono piu' scrittori sulla socket)
 *  \retval  -1    in tutti gl ialtri casi di errore (sets errno)
 *      
 */
int receiveMessage(int sc, message_t * msg);

/** scrive un messaggio sulla socket
 *   \param  sc file descriptor della socket
 *   \param msg struttura che contiene il messaggio da scrivere 
 *   
 *   \retval  n    il numero di caratteri inviati (se scrittura OK)
 *   \retval  SEOF se il peer ha chiuso la connessione 
 *                   (non ci sono piu' lettori sulla socket) 
 *   \retval -1   in tutti gl ialtri casi di errore (sets errno)
 
 */
int sendMessage(int sc, message_t *msg);

/** crea una connessione all socket del server. In caso di errore funzione tenta NTRIALCONN volte la connessione (a distanza di 1 secondo l'una dall'altra) prima di ritornare errore.
 *   \param  path  nome del socket su cui il server accetta le connessioni
 *   
 *   \return 0 se la connessione ha successo
 *   \retval SNAMETOOLONG se il nome del socket eccede UNIX_PATH_MAX
 *   \retval -1 negli altri casi di errore (sets errno)
 *
 *  in caso di errore ripristina la situazione inziale: rimuove eventuali socket create e chiude eventuali file descriptor rimasti aperti
 */
int openConnection(char* path);

/** Costruisce un messaggio a partire dal suo tipo e dalla sua lunghezza. La struttura è allocata dinamicamente
 *  per cui sta il programmatore deve deallocarla esplicitamente o tramite la funzione destroyMessage.
 *  
 *	\param t il tipo del messaggio. I tipi possibili sono specificati nel file comsock.h
 *  \param l la lunghezza del messaggio.
 * 	\param b puntatore al buffer da inserire nel messaggio
 * 
 *  \return un puntatore alla struttura in memoria heap o NULL in caso di errore
*/
message_t * createMessage(char t, unsigned int l, const char* b );


/** Distrugge un messaggio. La struttura deve essere stata allocata dinamicamente dal programmatore
 *  o tramite la funzione createMessage(). Assegna NULL al puntatore che prima puntava al messaggio.
 *  \param msg puntatore al messaggio da distruggere
 * 
 *  \return 0 se la funzione ha avuto successo, -1 altrimenti (sets errno)
*/
int destroyMessage(message_t** msg );

#endif
