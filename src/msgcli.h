/** \file msgcli.h
 * 	\brief Header per  il client MSGCli
 * 	\author Michele Carignani Mat. 439004
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 */


#include <signal.h>
#include <pthread.h>

#include "errors.h"

/*
#define DEBUG
*/

/*
#define COLORS
*/

/*
 #define NOTIFY
*/

#ifdef COLORS
	/** Codice ASCII per la formattazione rosso+bold */
	#define RED "\x1b[31;1m"
	/** Codice ASCII per la formattazione verde+bold */
	#define GREEN "\x1b[32;1m"
	/** Codice ASCII per la formattazione blu+bold */
	#define BLUE "\x1b[34;1m"
	/** Codice ASCII per la formattazione giallo+bold */
	#define YELLOW "\x1b[33;1m"
	/** Codice ASCII per la formattazione bianco+bold */
	#define WHITE "\x1b[0;1m"
	/** Codice ASCII per azzerare la formattazione */
	#define BLANK "\x1b[0;0m"
#else
	/** Codice ASCII per la formattazione rosso+bold */
	#define RED ""
	/** Codice ASCII per la formattazione verde+bold */
	#define GREEN ""
	/** Codice ASCII per la formattazione blu+bold */
	#define BLUE ""
	/** Codice ASCII per la formattazione giallo+bold */
	#define YELLOW ""
	/** Codice ASCII per la formattazione bianco+bold */
	#define WHITE ""
	/** Codice ASCII per azzerare la formattazione */
	#define BLANK ""
#endif

/** Percorso al file della socket del server MSG*/
#define SOCKNAME "./tmp/msgsock"

/** Limite massimo per la lunghezza di una riga di input */
#define MAXINPUT 70

/* Texts */
/** Testo di aiuto */
static char HelpString[] = "-- List of available commands --\n \
%LIST\t\t\tdisplay connected users\n \
%ONE <user> <message>\tsend <message> to <user>\n \
@ <user> <message>\tsend <message> to <user>\n \
%HELP\t\t\tdisplay this help message\n \
%EXIT\t\t\tclose connection and quit MSG Client\n\n";

/** Testo di apertura del programma */						
char WelcomeMessage[] = "---- Welcome in MSG Client :) ----\n \
Type %HELP for a list of available commands\n";
/** Testo di attesa di connessione */		
char ConnectionMessage[] = "--- Connecting... ---\n";
/** Testo di notifica della chiusura del server */
char ServerDown[] = "Il server ha chiuso la connessione\n";

/* Comandi disponibili */
/** Macro per i codici dei comandi disponibili */
enum Commands { COM_VOID, COM_ERROR, COM_BDCAST, COM_EXIT, COM_LIST, COM_ONE, COM_HELP, COM_AT};
/** Stringhe di invocazione di ciascun comando */
static char * CommandSet[] = { "%VOID" , "%ERROR",  "%BDCAST", "%EXIT", "%LIST", "%ONE", "%HELP", "@"};
/** */
static int CommandCodes[] = { COM_VOID, COM_ERROR, COM_BDCAST, COM_EXIT, COM_LIST , COM_ONE, COM_HELP, COM_AT };
/** Dimensione del set di comandi */
static int SetLength = 8;

/** ID del thread sender */
static pthread_t S;
/** ID del thread receiver */
static pthread_t R;
/** Variabile di notifica di chiusura */
volatile sig_atomic_t Closing;
