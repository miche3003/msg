/**	\file usersTable.c
 * 	\brief Funzioni per la gestione della tabella utenti
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 */


#include "common_hdrs.h"

#include "msgserv_defs.h"
#include "usersTable.h"

void* copyUserInfo(void* a){
	userinfo *new;
	
	if (a == NULL){ errno = EINVAL; return NULL;}
	
	new = (userinfo*) malloc(sizeof(userinfo));
	if (new == NULL) return NULL;
	new->connected = ((userinfo*)a)->connected;
	strcpy(new->nick, ((userinfo*)a)->nick);
	new->sockfd = ((userinfo*)a)->sockfd;
	
	return new;
}

int compare_string(void *a, void *b) {
    char *_a, *_b;
    _a = (char *) a;
    _b = (char *) b;
    return strcmp(_a,_b);
}

void* copyKey(void* a){
	return (void *) strdup(( char * ) a );
}

int CheckNickName(char* n){
	int i, k;
	for (i = 0; n[i] != '\0'; i++){
		k = (int) n[i];
		if ( !(( k >= 48) && (k <= 57))  /* Cifre decimali*/
		&& !((k >= 65) && (k <= 90))  /* Lettere maiuscole */
		&& !( (k >= 97) && (k <= 122))   /* Lettere minuscole */
		) {
			errno = EBADE;
			return -1;
		}
	}
	return 0;
}

/** Carica gli utenti in memoria in una tabella hash
 * 
 */
int LoadUsers (FILE* src_file, hashTable_t** table ) {
	userinfo* ui;
	char* u, *p;
	
	*table = new_hashTable(TABLESIZE, &compare_string, &copyKey, &copyUserInfo, &hash_string);
	u = (char*) malloc(sizeof(char) * MAXNICKLEN);
	while( fgets(u,MAXNICKLEN, src_file) != NULL){
		if ( (p = index(u, '\n')) != NULL) { *p = '\0'; }
		EC_NEG1( CheckNickName(u) )
		EC_NULL( p )
		
		/* Crea la nuova userinfo */
		ui = (userinfo*) malloc(sizeof(userinfo));
		
		/* All'avvio tutti gli utenti sono disconnessi */
		ui->connected = false;
		
		strcpy(ui->nick, u);
		
		add_hashElement(*table, u, ui);
		free(ui);
	}
	
	free(u);
	return 0;
	
	EC_CLEANUP_BGN
	printf("Invalid name list (%s): Only letters and numbers, max 256 chars\n",u);
	free(u);
	free_hashTable(table);
	errno = EBADE;
	return -1;
	EC_CLEANUP_END
}

/** Distrugge la tabella utenti
 * 
 */
int DestroyUsersTable(hashTable_t* Tab){
	errno = 0;
	free_hashTable(&Tab);
	if ( errno != 0) { return -1; }
	return 0;
}


int table_query(hashTable_t* table, char* key, int* sock, pthread_mutex_t* mtx){
	userinfo* us; /* payload ritornato dalla ricerca */
	int res; /* esito della ricerca nella tabella hash */
	
	if (sock != NULL)
		*sock = -1; /* */
	
	us = (userinfo*) getPayload(table, key);
	
	if (us == NULL)  {
		/* La specifica non distingue il caso in cui l'utente non esista nella
			tabella da quello in cui l'utente non è connesso. Questa implementazione
			permette di distinguerlo, assegnando NOUSER invece che ABSENT in questo
			ramo if \todo scrivere meglio*/ 
		res = NOUSER; 
	} else if (us->connected == false) {
		res = USR_IS_OFF;
	} else {
		res = USR_IS_ON;
		if (sock != NULL)
			*sock = us->sockfd;
	}
	return res;
}

/**
 * 
 */
int setOffline(hashTable_t* table, list_t* list, char* key){
	userinfo* us;
	
	us = (userinfo*) getPayload(table, key);
	
	if(us == NULL) return -1; 
	
	us->connected = false;
	remove_ListElement(list, key);
	
	return 0;
}

int setOnline(hashTable_t* table, list_t* list,char* key, int s){
	userinfo* us;
	
	us = (userinfo*) getPayload(table, key);
	
	if(us == NULL) return -1; 
	
	us->connected = true;
	us->sockfd = s;
	us->tid = pthread_self();
	add_ListElement(list, key, us);
	
	return 0;
}
