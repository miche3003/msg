/**\file msgsrv.c
   \author Michele Carignani Mat. 439004
   \brief MSG Server main implementation file (contains main() function )
	
	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
	
   Usage
   $ ./msgserv users_file log_file
 */

#include "msgsrv.h"

int ServerInit(){
	FILE* UsersFile; /* File contenente gli utenti ammessi */
	
	UsersFile = NULL;
	LogFile  = NULL;
	
	/* Funzione per l'installazione dei gestori e delle maschere dei segnali */
	EC_NEG1( SetSigHandlers( CloseUpHandler ) )
	
	/* La variabile di terminazione inizializzata a false */
	TimeToClose = false;
	
	/* Inizializzazione dei mutex e delle conditions */
	pthread_mutex_init(&TAB, NULL);

	/* Apertura file utenti */
	EC_NULL( UsersFile = fopen((char*) UserFilePath, "r") )
	
	/* Creazione e popolazione tabella hash*/
	EC_NEG1( LoadUsers(UsersFile, &UsersTable) )	
	fclose(UsersFile);
	UsersFile = NULL;
	
	/* Creazione lista utenti connessi */
	Broadcast = new_List(&compare_string, &copyKey, &copyUserInfo);
	
	/* Apertura del file di log */
	EC_NULL( LogFile = fopen(LogFilePath, "w") )
	
	/* Ancora non è connesso alcun utente */
	Connected = 0;
	
	return 0;
	
	EC_CLEANUP_BGN
		if (LogFile != NULL) 
			(void) fclose(LogFile);
			
		if (UsersFile != NULL) 
			(void) fclose(UsersFile);
			
		exit(EXIT_FAILURE);
	EC_CLEANUP_END
	
}

/** \brief Operazioni per la corretta terminazione del server MSG. 
 *  
 * 	La funzione provoca ed accerta la terminazione dei thread Worker e del thread Writer, dealloca la tabella UsersTable
 *  e la lista Broadcast, chiude il file di log, chiude e rimuove dal sistema la socket.
 * 	
 */
int ServerOff(int ServerSocket){
	pthread_t th;
	int *s, i;
	
	i = 0;
	while(i < MAXCLIENTS){
		EC_RETVAL( pthread_mutex_lock(&TAB) )
		if (Broadcast->head == NULL) {  break; }
		th = (( (userinfo*) (Broadcast->head)->payload)->tid);
		EC_RETVAL( pthread_kill(th, SIGUSR1) )
		EC_RETVAL( pthread_mutex_unlock(&TAB) )
		EC_RETVAL( pthread_join(th, (void**) &s) )
		i++;
	}
	
	closeSocket(ServerSocket);	

	/* Comunico la chiusura al thread Writer, inserendo in log_buffer un evento di tipo MSG_EXIT 
	  dopodichè aspetto la terminazione del thread */
	LogEvent(MSG_EXIT_EV, "", "", "");
	EC_RETVAL( pthread_join(writer, (void**) &s) )
	
	/* Deallocazione della tabella utenti */
	DestroyUsersTable(UsersTable);
	
	free_List(&Broadcast);
	
	if (LogFile != NULL)
		EC_RETVAL( fclose(LogFile) )
	
	closeSocket(ServerSocket);
	
	EC_NONZERO( unlink(SOCKET_NAME) )
	
	write(1, " MSG Server : quitting\n", 23 );
	
	return 0;
	
	EC_CLEANUP_BGN
		(void) fclose(LogFile);
		(void) unlink(SOCKET_NAME);
		write(1, " MSG Server : quitting\n", 23 );
		return -1;
	EC_CLEANUP_END
}

/**
 *  \brief Main function - avvia il server, inizializza le strutture dati condivise,
 * 	crea il thread writer e si blocca aspettando le connessioni di nuovi utenti.
 * 
 */
int main(int argc, char* argv[]){
	int Client, /* Fd della socket di un nuovo client connesso */
		ServerSocket; /* Fd delle socket lato server */
	message_t* msg; /* Messaggio */
	pthread_t newWorker; 
	char too_much[1000000];
	
	too_much[655] = 'a';
	
	/* Controllo degli argomenti da riga di comando*/
	if (argc != 3) {
		fprintf(stderr,"Utilizzo corretto: %s file_utenti file_log\n",argv[0]);
		exit(1);
	}
	
	/* Non è possibile cercare di validare i valori passati da riga di comando, essendo possibili
		tutti in caratteri per il nome di un file. Valori invalidi vengono gestiti al momento dell'apertura */
	LogFilePath = argv[2];
	UserFilePath = argv[1];

	/* Inizializzazione delle strutture del server */
	ServerInit(); /* TODO errori?!? */
	
	/* Se non esiste la cartella ./tmp la creo, poi attivo la socket */
	if ( mkdir( SOCKET_FOLDER, S_IRWXU ) == -1 )
		if (errno != EEXIST){
			perror("Cannot create ./tmp folder");
			goto ec_cleanup_bgn;
		}
	
	EC_NEG1( (ServerSocket = createServerChannel(SOCKET_NAME)) )
	
	/* Creazione del thread Writer che si occupa di scrivere il logfile */
	 EC_NONZERO( pthread_create(&writer, NULL, &Writer, LogFile) )
	 
	 /*
	 EC_NONZERO( pthread_create(&sigshield, NULL, &SigHandler, NULL) )
	 pthread_detach(sigshield); 
	*/
	while( !TimeToClose ){
		
		if( (Client = acceptConnection(ServerSocket)) != -1){ /* Bloccante */
			
			if (Connected < MAXCLIENTS){
					if (pthread_create(&newWorker, NULL, &Worker, (void*) &Client) != 0) {
						perror("Creazione thread");
					}
			} else {
				/* Notifica all'utente che il server è pieno con
				 * una politica best effort: in caso di errori chiude semplicemente
				 * la socket, senza comunicazione esplicita  */
				if ( (msg = createMessage(MSG_OK, 0, NULL)) != NULL){
					(void) receiveMessage(Client, msg);
					(void) destroyMessage(&msg);
					msg = createMessage(MSG_ERROR, (unsigned int) strlen(TooMany), TooMany);
					if ( msg != NULL){
						(void) sendMessage(Client, msg);
						(void) destroyMessage(&msg);
						(void) closeSocket(Client);
					} else {
						(void) closeSocket(Client);
					}	
				} else { /* Errore nella creazione del messaggio di ricezione: chiude */
					(void) closeSocket(Client);
				}
				
			}
			
		}
		
	}
	
	(void) ServerOff( ServerSocket );
	
	exit(EXIT_SUCCESS);
	
	EC_CLEANUP_BGN
	(void) ServerOff;
	exit(EXIT_FAILURE);
	EC_CLEANUP_END
}

void CloseUpHandler(int signum){
	TimeToClose = 1;
	return;
}
