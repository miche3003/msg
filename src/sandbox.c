#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include "errors.h"

char* makeBody(char* src, char* nick){
	char *sub, *body;
	int length, i, j, nick_l, src_l;
	
	/* lunghezza del corpo
	 * 		[ + nickname + ] + spazio + corpo del messaggio + \0
	 * 		1      strlen()     1       1                 strlen()                 1   = 4 + strlen() + strlen()
	 * */
	 
	if (src == NULL || nick == NULL ){ errno = EINVAL; return NULL; }
	
	src_l =strlen(src) + 1;
	nick_l =strlen(nick) + 1;
	length = 4 +  src_l + nick_l;
	
	MALLOC( body = (char*) malloc(sizeof(char)*length));
	
	/* Insert prefix - "[<mittente>]" */
	body[0] = '[';
	for(i = 1, j = 0; i < (nick_l); i++, j++){
		body[i] = nick[j];
	}
	body[i] = ']'; i++;
	body[i] = ' '; i++;
	
	/* copying message content*/
	for(j = 0; j < (src_l - 1); i++, j++){
		body[i] = src[j];
	}
	body[i] = '\0';
	
	return body;
}

void color_white_spaces(char* s){
	int i;
	for(i = 0; i <= strlen(s); i++){
		if(s[i] == ' ')
			s[i] = '/';
	}
	return;
}

int main(){
	int i, j, k;
	char *s1, *s2, *s3;
	
	char *nics[] = {
		"pippo",
		"pluto",
	};
	
	char *str[] = {
		"ciao come va?",
		"tutto bene grazie e te?",
		"  ",
	};
	
	s1 = makeBody(str[0], nics[0]);
	color_white_spaces(s1);
	printf("%s\n", s1);
	s2 = makeBody(str[1], nics[1]);
	color_white_spaces(s2);
	printf("%s\n", s2);
	s3 = makeBody(str[2], nics[0]);
	color_white_spaces(s3);
	printf("%s\n", s3);
	
	for(k = 0; k < 10; k++){
		printf("%c%c", '\0', 'a');
	 }
	printf("\n");
	
	
	free(s1);
	free(s2);
	free(s3);
	
  return 0;
}
