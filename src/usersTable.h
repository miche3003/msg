/** \file usersTable.h
 * 	\brief Funzioni per la gestione della tabella utenti
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 */

#include <stdio.h>
#include <pthread.h>
#include "genHash.h"

#ifndef _USERSTABLE_H
#define _USERSTABLE_H
/** Macro per i risultati di visita della tabella utenti */
enum hash_visit_results { NOUSER = 1, USR_IS_OFF, USR_IS_ON };

/** Lunghezza massima per un nickname */
#define MAXNICKLEN 256

/** La tabella hash contenente tutti gli utenti abilitati alla chat e il loro stato di connessione 
 *  NOTA: il campo sock è significativo solo se il campo connected vale 1
 */
typedef struct {
	/** Stato di connessione dell'utente */
	int connected;
	/** File descriptor della socket su cui è connesso l'utente */
	int sockfd;
	/** ID del worker che gestisce le richieste dell'utente */
	pthread_t tid;
	/** Nickname dell'utente */
	char nick[MAXNICKLEN];
} userinfo;

/** Verifica la validità di un nickname, ovvero che contenga solo caratteri alfanumerici
 *  \param n nickname da validare
 * 
 *  \return 0 se il nickname è valido, 1 altrimenti
 */
int CheckNickName(char* n);

/** Funzione per il confronto di stringhe
 */
int compare_string(void *a, void *b);

/** Funzione di copia della key stringa
 * 
 */
void* copyKey(void* a);

/** \brief Funzione di copia di una struttura UserInfo. La copia è allocata con malloc()
 * 
 * 	\param a puntatore alla struttura da copiare
 * 
 * 	\return puntatore alla struttura creata.
 */
void* copyUserInfo(void* a);

int DestroyUsersTable(hashTable_t* Tab);

int LoadUsers (FILE* UserFile, hashTable_t** table);

/** \brief Esegue una ricerca nella tabella degli utenti e ne salva il file descriptor della socket
 * 	
 *	\param table la tabella utenti in cui cercare
 * 	\param key la chiave per la ricerca (il nickname)
 * 	\param sock indirizzo in cui lasciare il fd della socket su cui è connesso l'utente.
 * 	\param mtx indirizzo in cui lasciare il mutex della socket del client
 * 
 * 	\return NOUSER, USR_IS_ON o USR_IS_OFF come definiti in usersTable.h
 * 
 * 	NOTA: al ritorno dalla funzione il valore di *sock è -1 se la ricerca ha dato esito ABSENT
 * 	
 */
int table_query(hashTable_t* table, char* key, int *sock, pthread_mutex_t *mtx);

/** Modifica lo stato dell'utente dall'username key come disconnesso
 * 
 * 	\param table tabella utenti contenente l'utente da modificare
 * 	\param list lista degli utenti connessi
 * 	\param key  nickname dell'utente
 * 
 * 	\return 0 on success
 */
int setOffline(hashTable_t* table, list_t* list, char* key);

/** Modifica lo stato dell'utente dall'username key come connesso
 * 
 * 	\param table tabella utenti contenente l'utente da modificare
 * 	\param list lista degli utenti connessi
 * 	\param key  nickname dell'utente
 * 	\param s fd della socket su cui è connesso l'utente
 * 
 * 	\return 0 on success
 * 
 */
int setOnline(hashTable_t* table, list_t* list, char* key, int s);

#endif
