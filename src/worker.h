/** \file worker.h
 *  \brief Funzioni adoperate per i thread Worker del server
 * 	\author Michele Carignani Mat. 439004
 * 
 * 	Il contenuto di questo file e' in ogni sua parte opera originale dell' autore
 * 
 */
 #ifndef WORKER_H_
 #define WORKER_H_
 
#include "common_hdrs.h"
#include "msgserv_defs.h"
#include "usersTable.h"

/** Parsa il contenuto di un messaggio di tipo MSG_TO_ONE separando il destinatario e il contenuto del messaggio. 
  *  Il formato della stringa src da analizzare segue la specifica del progetto, per cui in sostanza la funzione copia la prima 
  *  sequenza di caratteri escluso ' ' di src su dest e la seconda sequenza su content.
  *  
  * La funzione alloca dinamicamente una stringa il cui indirizzo viene assegnato a dest che conterrà il destinatario
  *  
  *		\param src la stringa da cui estrarre il destinatario
  * 	\param dest puntatore che conterrà l'indirizzo della stringa allocata contenente il destinatario
  * 	\param content puntatore che conterrà l'indirizzo del primo carattere del corpo del messaggio all'interno di src
  * 
  * 	\retval 0 se ha successo
  * 	\retval -1 in caso di argomenti non validi o contenuto di src invalido
  */
int parsePrivMsgBuff(char* src, char** dest, char** content);

/** \brief Costruisce il corpo del messaggio da inviare in caso di richiesta MSG_BROADCAST e MSG_TO_ONE.
  *  Alloca memoria con malloc() per il corpo del messaggio
  * 
  *		\param src la stringa da cui estrarre il corpo del messaggio
  * 	\param nick stringa contenente il nickname del mittente
  * 
  * 	\return puntatore alla stringa allocata contenente il corpo del messaggio
  * 	\return NULL in caso di errore (assegna errno)
  */
char* makeBody(char* src, char* nick );

/** Gestisce l'invio di un messaggio privato
 *   Variabili Globali : TAB, UsersTable
 * 
 * 	\param Csock la socket del mittente del messaggio
 * 	\param nickname il nickname del mittente
 * 	\param msg_in il messaggio ricevuto dal mittente
 * 
 * 	\return 0 se ha successo
 * 	\return -1 se fallisce
 */
int sendPriv(int Csock, char* nickname, message_t* msg_in);

/** \brief Costruisce il corpo per i messaggi MSG_LIST
 * 
 * 	Globali utilizzate: TAB, Broadcast
 *  Alloca la stringa dinamicamente con malloc().
 * 	 
 * 	\return puntatore ad una stringa contenente la lista degli utenti
 *  \return NULL in caso di errore
 */
char* createUsersList();

/** Gestisce l'invio dei messaggi di tipo broadcast
 * 	
 * 	
 *  
 */
int sendToAll(message_t* msg, char* nickname, char* content);

/** Verifica l'autenticazione di una nuova connessione con un client. In particolare implementa il protocollo di connessione
 *  e l'esistenza dell'utente fra quelli ammessi.
 * 	\param s la socket attravero cui è stata stabilita la connessione con il client
 * 	\param nick puntatore alla memoria in cui verrà scritto il nickname dell'utente connesso
 * 	\param mtx riferimento cui verrà associato l'indirizzo del semaforo del client
*/
int authVerify(int s, char** nick, pthread_mutex_t* mtx);


/** Descrive il comportamento del thread Worker, che gestisce una connessione con un
 * 	client MSG.
 * \param arg deve passare la socket su cui è connesso il nuovo client
 * 
 *  \return (void*) 0
 */
void* Worker(void * arg);

int LogEvent(char type, char* mitt, char* dest, char* body);
#endif
